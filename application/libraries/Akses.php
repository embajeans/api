<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Akses Library
============================================================== */
class Akses {
    public function get_akses($method) {
        $has_access = $this->route_app($method);
        return $has_access;
    }
    
    protected function route_app($method){
        // path => lokasi model
        // route => lokasi function di dalam model
        
        $response = array(
            array('kode' => 'get_location', 'model' => 'admin', 'path' => 'admin', 'route' => 'get_location'),
            array('kode' => 'get_kodepos', 'model' => 'admin', 'path' => 'admin', 'route' => 'get_kodepos'),
            array('kode' => 'propinsi', 'model' => 'admin', 'path' => 'admin', 'route' => 'data_propinsi'),
            array('kode' => 'generate_propinsi', 'model' => 'admin', 'path' => 'admin', 'route' => 'generate_propinsi'),
            array('kode' => 'generate_kota', 'model' => 'admin', 'path' => 'admin', 'route' => 'generate_kota'),
            array('kode' => 'auth', 'model' => 'auth', 'path' => 'auth', 'route' => 'login'),
            array('kode' => 'login', 'model' => 'login', 'path' => 'customer/login', 'route' => 'login'),
            array('kode' => 'aktivasi', 'model' => 'login', 'path' => 'customer/login', 'route' => 'aktivasi'),
            array('kode' => 'registrasi', 'model' => 'login', 'path' => 'customer/login', 'route' => 'registrasi'),
            array('kode' => 'produk_detail', 'model' => 'produk', 'path' => 'customer/produk', 'route' => 'data_produk_detail'),
            array('kode' => 'cek_ongkir', 'model' => 'ongkir', 'path' => 'customer/ongkir', 'route' => 'cek_ongkir'),
            array('kode' => 'add_to_cart', 'model' => 'cart', 'path' => 'customer/cart', 'route' => 'add_to_cart'),
            array('kode' => 'get_cart', 'model' => 'cart', 'path' => 'customer/cart', 'route' => 'get_cart'),
            array('kode' => 'update_cart', 'model' => 'cart', 'path' => 'customer/cart', 'route' => 'update_cart'),
            array('kode' => 'delete_cart', 'model' => 'cart', 'path' => 'customer/cart', 'route' => 'delete_cart'),
            
            array('kode' => 'flash_sale', 'model' => 'flashsale', 'path' => 'customer/flashsale', 'route' => 'flash_sale'),
            
            
            array('kode' => 'produk_attribute', 'model' => 'produk', 'path' => 'customer/produk', 'route' => 'produk_attribute'),
            array('kode' => 'produk_terbaru', 'model' => 'produk', 'path' => 'customer/produk', 'route' => 'produk_terbaru'),
            array('kode' => 'produk_kategori', 'model' => 'produk', 'path' => 'customer/produk', 'route' => 'produk_kategori'),
            array('kode' => 'produk_relasi', 'model' => 'produk', 'path' => 'customer/produk', 'route' => 'produk_relasi'),
            
            
            
            array('kode' => 'shop', 'model' => 'produk', 'path' => 'customer/produk', 'route' => 'shop'),
            array('kode' => 'payment', 'model' => 'transaksi', 'path' => 'customer/transaksi', 'route' => 'payment'),
            array('kode' => 'invoice', 'model' => 'transaksi', 'path' => 'customer/transaksi', 'route' => 'invoice'),
            
            
            // akun
            array('kode' => 'setting_akun', 'model' => 'akun', 'path' => 'customer/akun', 'route' => 'setting_akun'),
            array('kode' => 'edit_akun', 'model' => 'akun', 'path' => 'customer/akun', 'route' => 'edit_akun'),
            array('kode' => 'transaksi_akun', 'model' => 'akun', 'path' => 'customer/akun', 'route' => 'transaksi_akun'),
            array('kode' => 'konfirmasi_akun', 'model' => 'akun', 'path' => 'customer/akun', 'route' => 'konfirmasi_akun'),
            
            
            array('kode' => 'email_order', 'model' => 'notifikasi', 'path' => 'customer/notifikasi', 'route' => 'email_order'),
            
        );
        return $this->searchArrayKeyVal("kode", $method, $response);
    }
    
    protected function searchArrayKeyVal($sKey, $id, $array) {
        foreach ($array as $key => $val) {
            if ($val[$sKey] == $id) {
                return $val['model']."#".$val['path']."#".$val['route'];
            }
        }
        return false;
    }
}