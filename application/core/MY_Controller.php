<?php
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Site_Controller as CI Controller
============================================================== */
class Site_Controller extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->data = array();
    }
    
    public function response_sukses($param, $detail = ""){
        $data = array(
            "status" => "00",
            "description" => "SUCCESS",
            "data" => $param,
            "detail" => $detail
        );
        return json_encode($data);
    }
    
    public function response_gagal($rc, $ket, $resp = ""){
        $data = array(
            "status" => $rc,
            "description" => $ket,
            "data" => $resp
        );
        return json_encode($data);
    }
}