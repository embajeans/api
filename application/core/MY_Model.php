<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class 
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Core Model
============================================================== */
MY_Model extends CI_Model {

    public $db_prod;
    public $db_devel;
    private $email_sender;
    private $email_sender_password;
    
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    
        $this->db_prod = $this->load->database('default', TRUE);
        $this->db_devel = $this->load->database('devel', TRUE);
        
//        $this->email_sender = "secure.system.embajeans@gmail.com";
//        $this->email_sender_password = "embajeansforever";
        
        $this->email_sender = "bobbyadnanbb@gmail.com";
        $this->email_sender_password = "rahasia123$";
    }
    
    public function get_origin_default(){
        return "256";
    }
    
    public function response_sukses($param, $detail = ""){
        $data = array(
            "status" => "00",
            "description" => "SUCCESS",
            "data" => $param,
            "detail" => $detail
        );
        return json_encode($data);
    }
    
    public function response_gagal($rc, $ket, $resp = ""){
        $data = array(
            "status" => $rc,
            "description" => $ket,
            "data" => $resp
        );
        return json_encode($data);
    }
    
    public function send_shipping_get($url, $key){
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: $key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }
    
    public function send_shipping_post($url, $key, $param){
        $data = http_build_query($param);
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
              "content-type: application/x-www-form-urlencoded",
              "key: $key"
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }
    
    public function cek_produk_flash_sale($id_produk){
        $query = "SELECT f.flash_sale_id, f.keterangan, f.date_start, f.date_end,
            fp.flash_sale_detail_id, fp.harga_sale as harga, fp.stok, fp.terjual,
            p.id_produk, p.slug
            from flash_sale_produk fp
            LEFT JOIN flash_sale f on fp.flash_sale_id = f.flash_sale_id
            LEFT JOIN produk p on fp.id_produk = p.id_produk
            WHERE p.is_active = '1'
            and p.id_produk in ($id_produk)
            and f.date_start <= ?
            and f.date_end >= ?";
        return $this->db_prod->query($query, array(date("Y-m-d H:i:s"),date("Y-m-d H:i:s")));
    }
    
    
    public function create_email($param){
        $data = array();
        $email_data = json_decode($param);
        $email_to = empty($email_data->email_to) ? "" : $email_data->email_to;
        $email_cc = empty($email_data->email_cc) ? "" : $email_data->email_cc;
        $email_bcc = empty($email_data->email_bcc) ? "" : $email_data->email_bcc;
        $email_subjek = empty($email_data->email_subjek) ? "" : $email_data->email_subjek;
        
        $template = empty($email_data->template) ? "" : $email_data->template;
        
        $data['info_text1'] = empty($email_data->info_text1) ? "" : $email_data->info_text1;
        $data['info_text2'] = empty($email_data->info_text2) ? "" : $email_data->info_text2;
        $data['info_text3'] = empty($email_data->info_text3) ? "" : $email_data->info_text3;
        $data['info_text4'] = empty($email_data->info_text4) ? "" : $email_data->info_text4;
        $data['info_text5'] = empty($email_data->info_text5) ? "" : $email_data->info_text5;
        $data['info_text6'] = empty($email_data->info_text6) ? "" : $email_data->info_text6;
        $data['info_text7'] = empty($email_data->info_text7) ? "" : $email_data->info_text7;
        $data['info_text8'] = empty($email_data->info_text8) ? "" : $email_data->info_text8;
        $data['info_text9'] = empty($email_data->info_text9) ? "" : $email_data->info_text9;
        $data['info_text10'] = empty($email_data->info_text10) ? "" : $email_data->info_text10;
        
        if($email_to == ""){
            return "01";die();
        }
        
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = $this->email_sender;
        $config['smtp_pass'] = $this->email_sender_password;
        $config['charset'] = 'iso-8859-1';
        
        $this->email->initialize($config);
        $this->email->set_mailtype('html');
        $this->email->set_newline("\r\n");
        
        $subjek = $email_subjek;
        
        $this->email->set_mailtype('html');
        $this->email->to(urldecode($email_to));
        $this->email->cc(urldecode($email_cc));
        $this->email->bcc(urldecode($email_bcc));
        $this->email->from('Emba Jeans - '.$email_subjek.' <'.$this->email_sender.'>', 'Emba Jeans');
        
        $message = $this->load->view('email/'.$template, $data, true);    
        
        $this->email->message($message);
        $this->email->subject($subjek);
        
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function processing_insert_data($table, $data_insert){
        $this->db_prod->insert($table, $data_insert);
        return $this->db_prod->insert_id();
    }
    
    public function processing_update_data($table, $key_name, $key_id, $data_update){
        try{
            $this->db_prod->update($table, $data_update);
            $this->db_prod->where($key_name, $key_id);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function processing_delete_data($table, $key_name, $key_id){
        try{
            $this->db_prod->where($key_name, $key_id);
            $this->db_prod->delete($table);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
}
?>