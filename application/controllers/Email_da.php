<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Email
============================================================== */
class Email extends Site_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $this->load->view("email/confirm_registrasi");
    }
}