<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : API controller
============================================================== */
class Api extends Site_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $req = json_decode(file_get_contents('php://input'));
        if(empty($req)){
            echo $this->response_gagal("01", "Wrong parameter");die();
        }
        
        if(empty($req->method)){
            echo $this->response_gagal("01", "Wrong Method");die();
        }
        
        if(empty($req->processing_code)){
            echo $this->response_gagal("01", "Wrong Processing Code");die();
        }
        
        $this->load->library('akses');
        $method = $req->method;
        $proc_code = $req->processing_code;
        
        $modelname = $this->akses->get_akses($method);
        if($modelname == ""){
            echo $this->response_gagal("01", "Unknown Method");die();
        }
        
        $parseModel = explode("#", $modelname);
        $nama_model = $parseModel[0];
        $path_model = $parseModel[1];
        $route_model = $parseModel[2];
        
        try{
            $this->load->model($path_model);
        } catch (Exception $ex) {
            echo $this->response_gagal("01", "Model App not exist");die();
        }
        
        if($method == "auth"){// login api
            $result = $this->$nama_model->$route_model($req);
        }else{
            // cek login api
            $this->load->model('auth');
            $cek_user = json_decode($this->auth->login($req));
            if($cek_user->status == "00"){
                $result = $this->$nama_model->$route_model($req);
            }else{
                echo $this->response_gagal("01", $cek_user->description);die();
            }
        }
        echo $result;
    }
}