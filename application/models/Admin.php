<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Admin model
============================================================== */
class Admin extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function data_propinsi($param){
        $data = array();
        $query = "SELECT * FROM propinsi order by nama asc";
        $result = $this->db_prod->query($query);
        if($result->num_rows() > 0){
            foreach($result->result() as $row){
                $data["id"][] = $result->id;
                $data["nama"][] = $result->nama;
            }
            return $this->response_sukses($data);
        }else{
            return $this->response_gagal("02", "Propinsi tidak tersedia");die();
        }
    }
    
    public function generate_propinsi($param){
        if(empty($param->param->id_biller)){
            return $this->response_gagal("02", "Biller kosong");die();
        }
        
        $id_biller = $param->param->id_biller;
        $url = "";
        $key = "";
        $param = array();
        if($id_biller == "1"){
            $query = "SELECT * FROM biller where id_biller = ?";
            $result = $this->db_prod->query($query, $id_biller);
            if($result->num_rows() > 0){
                $row_data = $result->row();
                $url = $row_data->uri;
                $key = $row_data->key;
            }else{
                return $this->response_gagal("02", "Biller tidak ditemukan");die();
            }
        }
        
        // call api
        $url = $url."province";
        $response = json_decode($this->send_shipping_get($url, $key));
        if($response->rajaongkir->status->code == "200"){
            $list = $response->rajaongkir->results;
            foreach($list as $row){
                $data_insert = array(
                    "id_propinsi" => $row->province_id,
                    "nama" => $row->province,
                );
                
                try{
                    $this->process_insert("propinsi", $data_insert);
                } catch (Exception $ex) {
                    // duplikat, tidak diinsert apaapun
                }
            }
            return $this->response_sukses("");
        }else{
            return $this->response_gagal("02", $response->rajaongkir->status->description);die();
        }
    }
    
    public function generate_kota($param){
        if(empty($param->param->id_biller)){
            return $this->response_gagal("02", "Biller kosong");die();
        }
        
        $id_biller = $param->param->id_biller;
        $url = "";
        $key = "";
        $param = array();
        if($id_biller == "1"){
            $query = "SELECT * FROM biller where id_biller = ?";
            $result = $this->db_prod->query($query, $id_biller);
            if($result->num_rows() > 0){
                $row_data = $result->row();
                $url = $row_data->uri;
                $key = $row_data->key;
            }else{
                return $this->response_gagal("02", "Biller tidak ditemukan");die();
            }
        }
        
        // call api
        $url = $url."city";
        $response = json_decode($this->send_shipping_get($url, $key));
        if($response->rajaongkir->status->code == "200"){
            $list = $response->rajaongkir->results;
            foreach($list as $row){
                $data_insert = array(
                    "id_propinsi" => $row->province_id,
                    "id_kota" => $row->city_id,
                    "nama" => $row->city_name,
                    "kodepos" => $row->postal_code,
                    "tipe" => strtolower($row->type),
                );
                
                try{
                    $this->process_insert("kota", $data_insert);
                } catch (Exception $ex) {
                    // duplikat, tidak diinsert apaapun
                }
            }
            return $this->response_sukses("");
        }else{
            return $this->response_gagal("02", $response->rajaongkir->status->description);die();
        }
    }
    
    public function get_location($param){
        $resp = array();
        $keyword = strtolower($param->param->keyword);
        $query = "SELECT p.id_propinsi, p.nama_propinsi,
            kk.id_kota, kk.nama_kota,
            kc.id_kecamatan, kc.nama_kecamatan
            FROM area_kecamatan kc
            LEFT JOIN area_kota kk ON kc.id_kota = kk.id_kota
            LEFT JOIN area_propinsi p on kk.id_propinsi = p.id_propinsi
            where MATCH(nama_kecamatan)
            AGAINST('+$keyword*' IN BOOLEAN MODE)  order by kc.nama_kecamatan asc LIMIT 6";
        $result = $this->db_prod->query($query);
        if($result->num_rows() > 0){
            foreach($result->result() as $row){
                $resp[] = array(
                    "id_propinsi" => $row->id_propinsi,
                    "id_kota" => $row->id_kota,
                    "id_kecamatan" => $row->id_kecamatan,
                    "value" => ucwords(strtolower($row->nama_propinsi)).", ".ucwords(strtolower($row->nama_kota)).", ".ucwords(strtolower($row->nama_kecamatan)),
                    "label" => ucwords(strtolower($row->nama_propinsi)).", ".ucwords(strtolower($row->nama_kota)).", ".ucwords(strtolower($row->nama_kecamatan)),
                );
            }
            return $this->response_sukses($resp);
        }else{
            return $this->response_gagal("02", "Lokasi tidak tersedia");die();
        }
    }
    
    public function get_kodepos($param){
        $resp = array();
        $id = strtolower($param->param->kecamatan_id);
        $query = "SELECT id_kelurahan, nama_kelurahan, kodepos FROM area_kelurahan  
            where id_kecamatan = ? order by nama_kelurahan asc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            foreach($result->result() as $row){
                $resp[] = array(
                    "id" => $row->id_kelurahan,
                    "nama" => $row->nama_kelurahan,
                    "kodepos" => $row->kodepos
                );
            }
            return $this->response_sukses($resp);
        }else{
            return $this->response_gagal("02", "Kodepos tidak tersedia");die();
        }
    }
    
    protected function process_insert($table, $data_insert){
        $this->db_prod->insert($table, $data_insert);
        return $this->db_prod->insert_id();
    }
    
}