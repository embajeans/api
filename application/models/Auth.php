<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Auth model
============================================================== */
class Auth extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function login($param){
        $data = array();
        $result = $this->get_data($param->id, $param->password);
        if($result->num_rows() > 0){
            $result = $result->row();
            $data["nama"] = $result->nama;
            $data["email"] = $result->email;
            $data["hp"] = $result->hp;
            return $this->response_sukses($data);
        }else{
            return $this->response_gagal("02", "Invalid Credential API");die();
        }
    }
    
    protected function get_data($id, $pass)
    {
        $query = "SELECT * FROM user_api where user_api_name = ? and user_api_password = ?";
        return $this->db_prod->query($query, array($id, hash('sha256', $pass)));
    }
}