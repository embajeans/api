<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Login model
============================================================== */
class Login extends MY_Model {
    
    protected $link_aktivasi;
    public function __construct() {
        parent::__construct();
        
        $this->link_aktivasi = "http://dev.embajeans.store/activation/";
    }
    
    public function login($param){
        $data = array();
        if($param->param->email == "" || $param->param->password == ""){
            return $this->response_gagal("02", "Username atau Password tidak cocok");die();
        }
        
        $result = $this->get_data($param->param->email, $param->param->password);
        if($result->num_rows() > 0){
            $result = $result->row();
            $status = $result->is_active;
            if($status == "0"){
                return $this->response_gagal("01", "Anda sudah terdaftar. Kami sudah mengirimkan email konfirmasi pendaftaran ke alamat email Anda. Silahkan konfirmasi pendaftaran Anda");die();
            }else if($status == "1"){
                $data["email"] = $result->email;
                $data["hp"] = $result->hp;
                $data["firstname"] = $result->firstname;
                $data["lastname"] = $result->lastname;
                return $this->response_sukses($data);
            }else{
                return $this->response_gagal("03", "Maaf, Id Anda terblokir. Silahkan hubungi customer service kami");die();
            }
        }else{
            return $this->response_gagal("02", "Username atau Password tidak cocok");die();
        }
    }
    
    protected function get_data($id, $pass)
    {
        $query = "SELECT * FROM customer where email = ? and pass_user = ?";
        return $this->db_prod->query($query, array($id, hash('sha256', $pass)));
    }
    
    public function registrasi($param){
        $data = array();
        if(empty($param->param->email) || empty($param->param->password)){
            return $this->response_gagal("02", "Email atau password tidak tersedia. Silahkan masukkan kembali");die();
        }
        
        // cek email exist
        $query = "SELECT * FROM customer where email = ? LIMIT 1";
        $cek = $this->db_prod->query($query, array($param->param->email));
        if($cek->num_rows() > 0){
            $row = $cek->row();
            $status = $row->is_active;
            if($status == "0"){
                return $this->response_gagal("01", "Anda sudah terdaftar. Kami sudah mengirimkan email konfirmasi pendaftaran ke alamat email Anda.");die();
            }else if($status == "1"){
                return $this->response_gagal("02", "Anda sudah terdaftar, silahkan login");die();
            }else{
                return $this->response_gagal("03", "Maaf, Id Anda terblokir. Silahkan hubungi customer service kami");die();
            }
        }else{
            date_default_timezone_set('Asia/Jakarta');
            // insert data baru
            $data_insert = array(
                "customer_group_id" => 1,
                "email" => $param->param->email,
                "pass_user" => hash('sha256', $param->param->password),
                "date_added" => date('Y-m-d h:i:s'),
            );
            $insert = $this->process_insert($data_insert);
            if($insert){
                $param_email = array(
                    "email_to" => $param->param->email,
                    "email_cc" => "",
                    "email_bcc" => "",
                    "email_subjek" => "Konfirmasi pendaftaran akun emba jeans",
                    "template" => "confirm_registrasi",
                    "info_text1" => $this->link_aktivasi.$insert.'/'.hash('sha256', $param->param->password),
                );
                $status = $this->create_email(json_encode($param_email));
                if($status){
                    $data["email"] = $param->param->email;
                    return $this->response_sukses($data);
                }else{
                    // delete user not aktif
                    $this->db_prod->query("DELETE from customer where email = ?", array($param->param->email));
                    return $this->response_gagal("99", "Kami tidak dapat mengirimkan link aktivasi ke email Anda. Pastikan email Anda sudah terinput dengan benar." .$this->email->print_debugger());die();
                }
            }else{
                
                return $this->response_gagal("04", "Maaf, Registrasi gagal. Silahkan ulangi kembali.");die();
            }
        }
    }
    
    public function aktivasi($param){
        if(empty($param->param->id) || empty($param->param->password)){
            return $this->response_gagal("02", "Kode aktivasi yang Anda masukkan tidak benar atau sudah kadaluwarsa");die();
        }
        $query = "SELECT * FROM customer where customer_id = ? and pass_user = ? and is_active = '0' LIMIT 1";
        $cek = $this->db_prod->query($query, array($param->param->id, $param->param->password));
        if($cek->num_rows() > 0){
            $data_update = array(
                "is_active" => 1
            );
            $update = $this->process_update($param->param->id, $data_update);
            if($update){
                return $this->response_gagal("00", "Email Anda berhasil diaktifkan");die();
            }else{
                return $this->response_gagal("02", "Kode aktivasi yang Anda masukkan tidak benar atau sudah kadaluwarsa");die();
            }
        }else{
            return $this->response_gagal("02", "Kode aktivasi yang Anda masukkan tidak benar atau sudah kadaluwarsa");die();
        }
    }
    
    protected function process_insert($data_insert){
        $this->db_prod->insert("customer", $data_insert);
        return $this->db_prod->insert_id();
    }
    
    protected function process_update($id, $data_update){
        try{
            $this->db_prod->update("customer", $data_update);
            $this->db_prod->where("customer_id", $id);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
}