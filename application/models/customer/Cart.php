<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Cart model
============================================================== */
class Cart extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function add_to_cart($param){
        $data = array();
        if(empty($param->param->id_produk)){
            return $this->response_gagal("02", "Id Produk tidak tersedia");die();
        }
        if(empty($param->param->qty)){
            return $this->response_gagal("02", "Qty tidak tersedia");die();
        }
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        if(empty($param->param->size)){
            return $this->response_gagal("02", "Size tidak tersedia");die();
        }
        if(empty($param->param->color)){
            return $this->response_gagal("02", "Warna tidak tersedia");die();
        }
        
        $data['customer_email'] = $param->param->email;
        $data['customer_produk'] = $param->param->id_produk;
        $data['customer_qty'] = $param->param->qty;
        $data['customer_size'] = $param->param->size;
        $data['customer_color'] = $param->param->color;
        $data['customer_sess_id'] = $param->param->session_id == "" ? "" : $param->param->session_id;
        
        $query = "SELECT * FROM customer where email = ? and is_active = '1'";
        $result = $this->db_prod->query($query, $data['customer_email']);
        if($result->num_rows() > 0){
            $result = $result->row();
            $customer_id = $result->customer_id;
            
            // checking stok
            $stok_sekarang = 0;
            $last_stok = $this->get_stok_produk($customer_id, $data['customer_produk'], $data['customer_size']);
            if($last_stok){
                $stok_sekarang = intval($last_stok) - intval($data['customer_qty']);
                if(intval($stok_sekarang) < 0){
                    return $this->response_gagal("02", "Maaf, produk tidak dapat ditambahkan ke dalam keranjang belanja. jumlah barang melebihi stok saat ini");die();
                }
            }else{
                return $this->response_gagal("02", "Maaf, produk tidak dapat ditambahkan ke dalam keranjang belanja. stok tidak tersedia");die();
            }
            // cek is exist cart
            if($data['customer_email'] != "adminemba@gmail.com"){
                $cart = "SELECT * FROM order_cart where customer_id = ? 
                    and id_produk = ?
                    and size = ?
                    and color = ?";
                $result_cart = $this->db_prod->query($cart, array($customer_id, $data['customer_produk'], $data['customer_size'], $data['customer_color']));
            }else{
                $cart = "SELECT * FROM order_cart where customer_id = ? 
                    and id_produk = ?
                    and size = ?
                    and color = ?
                    and session_id = ?";
                $result_cart = $this->db_prod->query($cart, array($customer_id, $data['customer_produk'], $data['customer_size'], $data['customer_color'], $data['customer_sess_id']));
            }
            if($result_cart->num_rows() > 0){
                // produk sudah ada, update qty di tabel cart
                $row_cart = $result_cart->row();
                $cart_id = $row_cart->cart_id;
                $qty = $row_cart->qty;
                $qty_update = intval($qty) + intval($data['customer_qty']);
                // update qty cart
                $data_update = array(
                    "qty" => $qty_update
                );
                $this->process_update($data_update, $cart_id);
                $data['customer_qty'] = $qty_update;
                
                // call get cart
                $req = array(
                    "param" => array(
                        "email" => $data['customer_email']
                    )
                );
                $list_cart = $this->get_cart(json_decode(json_encode($req)));
                return $list_cart;
            }else{
                // insert new cart
                $data_insert = array(
                    "id_produk" => $data['customer_produk'],
                    "size" => $data['customer_size'],
                    "color" => $data['customer_color'],
                    "customer_id" => $customer_id,
                    "session_id" => $data['customer_sess_id'],
                    "qty" => $data['customer_qty'],
                    "date_added" => date('Y-m-d H:i:s'),
                    "is_expired" => "0",
                );
                $insert_cart = $this->process_insert($data_insert);
                if($insert_cart){
                    // call get cart
                    $req = array(
                        "param" => array(
                            "email" => $data['customer_email']
                        )
                    );
                    $list_cart = $this->get_cart(json_decode(json_encode($req)));
                    return $list_cart;
                }else{
                    return $this->response_gagal("02", "Maaf, produk tidak dapat ditambahkan ke dalam cart");die();
                }
            }
        }else{
            return $this->response_gagal("02", "Email tidak tersedia atau tidak aktif");die();
        }
    }
    
    public function update_cart($param){
        $data = array();
        if(empty($param->param->id_cart)){
            return $this->response_gagal("02", "Id tidak tersedia");die();
        }
        if(empty($param->param->qty)){
            return $this->response_gagal("02", "Qty tidak tersedia");die();
        }
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        if(empty($param->param->size)){
            return $this->response_gagal("02", "Size tidak tersedia");die();
        }
        if(empty($param->param->color)){
            return $this->response_gagal("02", "Warna tidak tersedia");die();
        }
        $data['customer_email'] = $param->param->email;
        $data['customer_cart_id'] = $param->param->id_cart;
        $data['customer_qty'] = $param->param->qty;
        $data['customer_size'] = $param->param->size;
        $data['customer_color'] = $param->param->color;
        $data['customer_sess_id'] = $param->param->session_id == "" ? "" : $param->param->session_id;
        
        $cart = "SELECT * FROM order_cart where cart_id = ?";
        $result_cart = $this->db_prod->query($cart, $data['customer_cart_id']);
        if($result_cart->num_rows() > 0){
            // produk sudah ada, update qty di tabel cart
            $row_cart = $result_cart->row();
            $cart_id = $row_cart->cart_id;
            
            // update qty cart
            $data_update = array(
                "qty" => intval($data['customer_qty'])
            );
            
            try{
                $this->process_update($data_update, $cart_id);
                return $this->response_sukses("");
            } catch (Exception $ex) {
                return $this->response_gagal("02", "Maaf, gagal mengupdate barang di dalam keranjang. Silakan ulangi kembali");die();
            }
        }else{
            return $this->response_gagal("02", "Item keranjang belanja tidak ditemukan");die();
        }
    }
    
    public function delete_cart($param){
        $data = array();
        if(empty($param->param->id_cart)){
            return $this->response_gagal("02", "Id tidak tersedia");die();
        }
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        if(empty($param->param->size)){
            return $this->response_gagal("02", "Size tidak tersedia");die();
        }
        if(empty($param->param->color)){
            return $this->response_gagal("02", "Warna tidak tersedia");die();
        }
        $data['customer_email'] = $param->param->email;
        $data['customer_cart_id'] = $param->param->id_cart;
        $data['customer_size'] = $param->param->size;
        $data['customer_color'] = $param->param->color;
        $data['customer_sess_id'] = $param->param->session_id == "" ? "" : $param->param->session_id;
        
        $cart = "SELECT * FROM order_cart where cart_id = ?";
        $result_cart = $this->db_prod->query($cart, $data['customer_cart_id']);
        if($result_cart->num_rows() > 0){
            // produk sudah ada, update qty di tabel cart
            $row_cart = $result_cart->row();
            $cart_id = $row_cart->cart_id;
            
            // delete cart
            try{
                $this->db_prod->query("DELETE FROM order_cart where cart_id = ?", $cart_id);
                return $this->response_sukses("");
            } catch (Exception $ex) {
                return $this->response_gagal("02", "Maaf, gagal menghapus barang di dalam keranjang. Silakan ulangi kembali");die();
            }
        }else{
            return $this->response_gagal("02", "Item keranjang belanja tidak ditemukan");die();
        }
    }
    
    public function get_cart($param){
        $data = array();
        $data["customer_id"] = "";
        $data["customer_session_id"] = "";
            
        if($param->param->email == ""){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        
        if($param->param->email == "adminemba@gmail.com"){
            if($param->param->sesssion_id == ""){
                return $this->response_gagal("02", "Session id tidak tersedia");die();
            }else{
                $data["customer_session_id"] = $param->param->sesssion_id;
            }
        }
        
        
        $query = "SELECT * FROM customer where email = ? and is_active = 1 limit 1";
        $result = $this->db_prod->query($query, $param->param->email);
        if($result->num_rows() > 0){
            $result = $result->row();
            $customer_id = $result->customer_id;
            
            $data['customer_id'] = $result->customer_id;
            $data['customer_group_id'] = $result->customer_group_id;
            $data['customer_email'] = $result->email;
            $data['customer_firstname'] = $result->firstname;
            $data['customer_lastname'] = $result->lastname;
            $data['customer_hp'] = $result->hp;
            
            // get data alamat customer
            $data['address'] = array();
            $query_alamat = "SELECT c.customer_address_id, c.customer_id, c.id_propinsi, 
                c.id_kota, c.id_kecamatan, c.id_kecamatan, c.id_kelurahan, c.alamat,
                p.nama_propinsi,
                k.nama_kota,
                kc.nama_kecamatan,
                kl.nama_kelurahan, kl.kodepos
                FROM customer_address c 
                LEFT JOIN area_propinsi p ON c.id_propinsi = p.id_propinsi
                LEFT JOIN area_kota k ON c.id_kota = k.id_kota
                LEFT JOIN area_kecamatan kc ON c.id_kecamatan = kc.id_kecamatan
                LEFT JOIN area_kelurahan kl ON c.id_kelurahan = kl.id_kelurahan
                where c.customer_id = ?";
            $result_alamat = $this->db_prod->query($query_alamat, $customer_id);
            if($result_alamat->num_rows() > 0){
                foreach($result_alamat->result() as $row){
                    $data['address'][] = array(
                        "customer_address_id" => $row->customer_address_id,
                        "customer_id" => $row->customer_id,
                        "customer_propinsi_id" => $row->id_propinsi,
                        "customer_propinsi_name" => $row->nama_propinsi,
                        "customer_city_id" => $row->id_kota,
                        "customer_city_name" => $row->nama_kota,
                        "customer_kecamatan_id" => $row->id_kecamatan,
                        "customer_kecamatan_name" => $row->nama_kecamatan,
                        "customer_kelurahan_id" => $row->id_kelurahan,
                        "customer_kelurahan_name" => $row->nama_kelurahan,
                        "customer_kodepos" => $row->kodepos,
                        "customer_address" => $row->alamat,
                    );
                }
            }
            
            // get detail cart
            $query_produk = "SELECT c.cart_id, c.session_id, c.qty, c.is_expired, c.id_produk, c.qty, c.color as color_id,
                (
                    select v.nama from varian v 
                    left join produk_varian pv on v.id_varian = pv.id_varian
                    where v.id_varian = c.color and v.is_warna = '1' and v.is_active = '1'
                    and pv.id_produk = c.id_produk
                )as color, c.size as size_id,
                (
                    select v.nama from varian v 
                    left join produk_varian pv on v.id_varian = pv.id_varian
                    where v.id_varian = c.size and v.is_warna = '0' and v.is_active = '1'
                    and pv.id_produk = c.id_produk
                )as size, 
                p.sku, p.nama, p.slug, p.harga, p.harga_coret, p.berat
                FROM order_cart c
                left join produk p on c.id_produk = p.id_produk
                where c.customer_id = ?
                and c.is_expired = '0'
                and p.is_active = '1'";
            
            $querydiskon = "SELECT c.cart_id, c.session_id, c.qty, c.is_expired, c.id_produk, c.qty, c.color as color_id,
                (
                    select v.nama from varian v 
                    left join produk_varian pv on v.id_varian = pv.id_varian
                    where v.id_varian = c.color and v.is_warna = '1' and v.is_active = '1'
                    and pv.id_produk = c.id_produk
                )as color, c.size as size_id,
                (
                    select v.nama from varian v 
                    left join produk_varian pv on v.id_varian = pv.id_varian
                    where v.id_varian = c.size and v.is_warna = '0' and v.is_active = '1'
                    and pv.id_produk = c.id_produk
                )as size, 
                p.sku, p.nama, p.slug, p.harga as harga_asli, p.berat,
                pd.harga as harga_diskon
                FROM order_cart c
                left join produk p on c.id_produk = p.id_produk
                left join produk_diskon pd ON p.id_produk = pd.id_produk 
                where c.customer_id = ?
                and c.is_expired = '0'
                and p.is_active = '1'
                and pd.date_start <= ?
                and pd.date_end >= ?";

            $result_produk = $this->db_prod->query($query_produk, $customer_id);
            if($result_produk->num_rows() > 0){
                $data_produk_cart = array();
                $data_produk_cart_diskon = array();
                $produk_stok = array();
                $data_produk_flash = array();
                $list_id = "";
                foreach($result_produk->result() as $row){
                    $list_id .= $row->id_produk.",";
                    $data_produk_cart[] = array(
                        "produk_cart_id" => $row->cart_id,
                        "produk_id" => $row->id_produk,
                        "produk_slug" => $row->slug,
                        "produk_size_id" => $row->size_id,
                        "produk_size_name" => $row->size,
                        "produk_color_id" => $row->color_id,
                        "produk_color" => $row->color,
                        "produk_qty" => $row->qty,
                        "produk_name" => $row->nama,
                        "produk_image" => base_url().'assets/images/img-coming-soon.png',
                        "produk_image_thumbnail" => base_url().'assets/images/img-coming-soon.png',
                        "produk_weight" => $row->berat,
                        "produk_harga" => $row->harga,
                        "produk_harga_coret" => $row->harga_coret == "" ? "0":$row->harga_coret,
                        "produk_stok" => 0,
                        "produk_stok_detail" => 0,
                    );
                }
                $list_id = rtrim($list_id, ",");
                $result_diskon = $this->db_prod->query($querydiskon, array($customer_id, date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
                if($result_diskon->num_rows() > 0){
                    foreach($result_diskon->result() as $row){
                        $data_produk_cart_diskon[] = array(
                            "produk_id" => $row->id_produk,
                            "produk_harga" => $row->harga_diskon,
                            "produk_harga_coret" => $row->harga_asli
                        );
                    }
                }
                // compare harga produk asli dengan produk diskon
                foreach ($data_produk_cart as $i => $defArr) {
                    foreach ($data_produk_cart_diskon as $j => $dayArr) {
                        if ($dayArr['produk_id'] == $defArr['produk_id']) {
                            $data_produk_cart[$i]['produk_harga'] = $data_produk_cart_diskon[$j]['produk_harga'];
                            $data_produk_cart[$i]['produk_harga_coret'] = $data_produk_cart_diskon[$j]['produk_harga_coret'];
                        }
                    }
                }
                
                // produk image
                $result_img = $this->db_prod->query("SELECT * FROM produk_image where id_produk in($list_id) and sort_order = 1 group by id_produk");
                if($result_img->num_rows() > 0){
                    foreach($result_img->result() as $rowimg){
                        $produk_img[] = array(
                            "produk_id" => $rowimg->id_produk,
                            "produk_image" => $rowimg->path_image,
                            "produk_image_thumbnail" => $rowimg->path_image
                        );
                    }
                    // compare porduk dengan produk image
                    foreach ($data_produk_cart as $i => $defArr) {
                        foreach ($produk_img as $j => $dayArr) {
                            if ($dayArr['produk_id'] == $defArr['produk_id']) {
                                $data_produk_cart[$i]['produk_image'] = $produk_img[$j]['produk_image'];
                                $data_produk_cart[$i]['produk_image_thumbnail'] = $produk_img[$j]['produk_image_thumbnail'];
                            }
                        }
                    }
                }
                
                // produk stok detail
                $produk_stok_detail = array();
                $query_stok = "select p.id_produk, p.id_varian, p.stok, v.is_warna, v.nama, v.keterangan
                    from produk_stok p
                    left join varian v on p.id_varian = v.id_varian
                    where p.id_produk in ($list_id)";
                $result_stok = $this->db_prod->query($query_stok);
                if($result_stok->num_rows() > 0){
                    foreach($result_stok->result() as $row){
                        if($row->is_warna == "0"){
                            $produk_stok_detail[] = array(
                                "id_produk" => $row->id_produk,
                                "id_varian" => $row->id_varian,
                                "is_warna" => $row->is_warna,
                                "nama" => $row->nama,
                                "keterangan" => $row->keterangan,
                                "stok" => $row->stok,
                            );
                        }
                    }
                }
                
                
                // get stok and image vs qty cart
                $produk_stok = array();
                $qry_stok = "SELECT c.id_produk, sum(c.qty)as total_qty, sum(ps.stok) as total_stok
                    from order_cart c
                    left join produk p on c.id_produk = p.id_produk
                    left JOIN produk_stok ps on p.id_produk = ps.id_produk
                    where p.is_active = '1'
                    and c.customer_id = ?
                    and p.id_produk in ($list_id)
                    group by c.id_produk";
                $result_stok = $this->db_prod->query($qry_stok, $customer_id);
                foreach($result_stok->result() as $row){
                    $sisa_stok = intval($row->total_stok) - intval($row->total_qty);
                    $produk_stok[] = array(
                        "produk_id" => $row->id_produk,
                        "qty" => $row->total_qty,
                        "stok" => $sisa_stok,
                    );
                }
                
                
                // compare stok produk asli dengan produk diskon
                foreach ($data_produk_cart as $i => $defArr) {
                    foreach ($produk_stok as $j => $dayArr) {
                        if ($dayArr['produk_id'] == $defArr['produk_id']) {
                            $data_produk_cart[$i]['produk_stok'] = $produk_stok[$j]['stok'];
                        }
                    }
                }
                foreach ($data_produk_cart as $i => $defArr) {
                    foreach ($produk_stok_detail as $j => $dayArr) {
                        if ($dayArr['id_produk'] == $defArr['produk_id']) {
                            $stok_update = $produk_stok_detail[$j]['stok'];
                            if($produk_stok_detail[$j]['id_varian'] == $data_produk_cart[$i]['produk_size_id']){
                                $stok_update = intval($produk_stok_detail[$j]['stok']) - intval($data_produk_cart[$i]['produk_qty']);
                                $data_produk_cart[$i]['produk_stok_detail'] = $stok_update;
                            }

                        }
                    }
                }
                
                // get produk flash sale
                $qry_flash = "SELECT c.id_produk, sum(c.qty)as total_qty, 
                    f.flash_sale_id, f.keterangan, f.date_start, f.date_end,
                    fp.flash_sale_detail_id, fp.harga_sale as harga, fp.stok, fp.terjual
                    from order_cart c
                    left join produk p on c.id_produk = p.id_produk
                    left join flash_sale_produk fp on c.id_produk = fp.id_produk
                    LEFT JOIN flash_sale f on fp.flash_sale_id = f.flash_sale_id
                    where p.is_active = '1'
                    and c.customer_id = ?
                    and f.date_start <= ?
                    and f.date_end >= ?
                    group by c.id_produk";
                $flash = $this->db_prod->query($qry_flash, array($customer_id, date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
                if($flash->num_rows() > 0){
                    foreach($flash->result() as $row_flash){
                        $stok_produk = intval($row_flash->stok) - intval($row_flash->terjual);
                        $stok_produk = $stok_produk - intval($row_flash->total_qty);
                        $data_produk_flash[] = array(
                            "produk_id" => $row_flash->id_produk,
                            "produk_stok" => $stok_produk,
                            "produk_harga" => $row_flash->harga,
                        );
                    }
                    // compare stok produk asli dengan produk flash sale
                    foreach ($data_produk_cart as $i => $defArr) {
                        foreach ($data_produk_flash as $j => $dayArr) {
                            if ($dayArr['produk_id'] == $defArr['produk_id']) {
                                $data_produk_cart[$i]['produk_stok'] = $data_produk_flash[$j]['produk_stok'];
                                $data_produk_cart[$i]['produk_harga'] = $data_produk_flash[$j]['produk_harga'];
                            }
                        }
                    }
                }
                
                // compare product cart dengan cart
                $jml = 0;
                $berat = 0;
                $total = 0;
                foreach ($data_produk_cart as $row) {
                    $subberat = intval($row['produk_weight']) * intval($row['produk_qty']);
                    $subtotal = intval($row['produk_harga']) * intval($row['produk_qty']);
                    $berat += intval($subberat);
                    $total += intval($subtotal);
                    $jml += intval($row['produk_qty']);
                }
                
                $resp = array(
                    "jumlah" => $jml,
                    "berat" => $berat,
                    "total" => $total,
                    "produk" => $data_produk_cart        
                );
                return $this->response_sukses($resp, $data);
            }else{
                return $this->response_gagal("03", "Produk sudah tidak tersedia");
            }
        }else{
            return $this->response_gagal("02", "Email Anda belum terdaftar di sistem kami. Silahkan registrasi atau verivikasi email Anda terlebih dahulu");
        }
    }
   
    protected function get_stok_produk($customer_id, $produk_id, $id_varian){
        $sisa_stok = "";
        $qry_stok = "SELECT c.id_produk, sum(c.qty)as total_qty, 
            ps.stok as total_stok
            from order_cart c
            left join produk p on c.id_produk = p.id_produk
            left JOIN produk_stok ps on p.id_produk = ps.id_produk
            where p.is_active = '1'
            and c.customer_id = ?
            and c.id_produk = ?
            and ps.id_varian = ?
            group by c.id_produk";
        $result_stok = $this->db_prod->query($qry_stok, array($customer_id, $produk_id, $id_varian));
        if($result_stok->num_rows() > 0){
            $stok = $result_stok->row();
            $sisa_stok = intval($stok->total_stok) - intval($stok->total_qty);
            
            // cek produk flash sale
            $flash = $this->cek_produk_flash_sale($stok->id_produk);
            if($flash->num_rows() > 0){
                $row_flash = $flash->row();
                $stok_flash_sale = intval($row_flash->stok) - $row_flash->terjual;
                if(intval($stok_flash_sale) > 0){
                    $sisa_stok = intval($stok_flash_sale) - intval($stok->total_qty);
                }
            }
        }else{
            $qry_stok = "SELECT p.id_produk, ps.stok
            from produk p
            left JOIN produk_stok ps on p.id_produk = ps.id_produk
            where p.is_active = '1'
            and p.id_produk = ?";
            $result_stok = $this->db_prod->query($qry_stok, array($produk_id));
            if($result_stok->num_rows() > 0){
                $stok = $result_stok->row();
                $sisa_stok = $stok->stok;
                
                // cek produk flash sale
                $flash = $this->cek_produk_flash_sale($stok->id_produk);
                if($flash->num_rows() > 0){
                    $row_flash = $flash->row();
                    $sisa_stok = $row_flash->stok;
                }
            }
        }
        return $sisa_stok;
    }
    
    protected function process_insert($data_insert){
        $this->db_prod->insert("order_cart", $data_insert);
        return $this->db_prod->insert_id();
    }
    
    protected function process_update($data_update, $id){
        $this->db_prod->where("cart_id", $id);
        $this->db_prod->update("order_cart", $data_update);
    }
    
}