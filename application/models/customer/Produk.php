<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Produk model
============================================================== */
class Produk extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function produk_terbaru($param){
        $email = "";
        if(!empty($param->param->email)){
            $email = $param->param->email;
        }
        if(empty($param->param->tipe)){
            return $this->response_gagal("02", "Tipe tidak tersedia");die();
        }
        if(empty($param->param->limit)){
            return $this->response_gagal("02", "limit tidak tersedia");die();
        }
        
        $tipe = $param->param->tipe;
        $limit = $param->param->limit;
        
        $query = "SELECT p.id_produk, p.sku, p.nama, p.deskripsi, p.harga_coret, p.harga, p.info1, p.info2, p.date_added, p.berat, p.slug
            from produk p
            WHERE p.is_active = '1' ";
        
        if($tipe == "1"){// terbaru
            $sort_order = " order by p.date_added DESC LIMIT $limit";
        }else if($tipe == "2"){// termurah
            $sort_order = " order by p.harga ASC LIMIT $limit";
        }else{//termahal
            $sort_order = " order by p.harga DESC LIMIT $limit";
        }
        
        $result = $this->db_prod->query($query.$sort_order);
        if($result->num_rows() > 0){
            $data_produk = array();
            $list_id_produk = "";
            foreach($result->result() as $row){
                $list_id_produk .= $row->id_produk.",";
                $data_produk[] = array(
                    "produk_id" => $row->id_produk,
                    "produk_sku" => $row->sku,
                    "produk_slug" => $row->slug,
                    "produk_nama" => $row->nama,
                    "produk_harga" => $row->harga,
                    "produk_harga_coret" => $row->harga_coret == "" ? "0":$row->harga_coret,
                    "produk_date_added" => $row->date_added,
                    "produk_image" => "",
                    "produk_stok" => "100",//$row->stok,
                    "produk_weight" => $row->berat,
                );
            }
            $list_id_produk = rtrim($list_id_produk, ",");
            
            if($email != ""){
                $querydiskon = "SELECT p.id_produk, p.sku, p.nama, p.deskripsi, p.harga_coret, p.harga as harga_asli,  p.berat, 
                    p.info1, p.info2, p.date_added, pd.id_diskon, pd.harga
                    from produk p
                    left join produk_diskon pd ON p.id_produk = pd.id_produk 
                    left join customer c ON pd.customer_group_id = c.customer_group_id
                    where p.is_active = '1'
                    and p.id_produk in ($list_id_produk)
                    and pd.date_start <= now()
                    and pd.date_end >= now()
                    and c.email = ?";
                
                $result_diskon = $this->db_prod->query($querydiskon, $email);
                if($result_diskon->num_rows() > 0){
                    // ganti harga
                    $resultDiskon = $result_diskon->row();
                    $data["produk_harga"] = $resultDiskon->harga;
                    $data["produk_harga_coret"] = $resultDiskon->harga_asli;
                    
                    foreach($result_diskon->result() as $rowdiskon){
                        $data_produk_diskon[] = array(
                            "produk_id" => $rowdiskon->id_produk,
                            "produk_harga" => $rowdiskon->harga,
                            "produk_harga_coret" => $rowdiskon->harga_asli,
                        );
                    }
                    foreach ($data_produk as $i => $defArr) {
                        foreach ($data_produk_diskon as $j => $dayArr) {
                            if ($dayArr['produk_id'] == $defArr['produk_id']) {
                                $data_produk[$i]['produk_harga'] = $data_produk_diskon[$j]['produk_harga'];
                                $data_produk[$i]['produk_harga_coret'] = $data_produk_diskon[$j]['produk_harga_coret'];
                            }
                        }
                    }
                }
            }
            
            // get image produk
            $query_img = "SELECT pi.id_produk, pi.path_image, pi.sort_order from produk_image pi where id_produk in ($list_id_produk) and sort_order = 1";
            $result_img = $this->db_prod->query($query_img);
            if($result_img->num_rows() > 0){
                foreach($result_img->result() as $rowimg){
                    $data_produk_img[] = array(
                        "produk_id" => $rowimg->id_produk,
                        "image" => $rowimg->path_image,
                        "sort_order" => $rowimg->sort_order,
                    );
                }
                foreach ($data_produk as $i => $defArr) {
                    foreach ($data_produk_img as $j => $dayArr) {
                        if ($dayArr['produk_id'] == $defArr['produk_id']) {
                            $data_produk[$i]['produk_image'] = $data_produk_img[$j]['image'];
                        }
                    }
                }
            }
            return $this->response_sukses($data_produk);
        }else{
            return $this->response_gagal("02", "Produk belum tersedia");die();
        }
    }
    
    public function produk_relasi($param){
        $kategori = array();
        
    }
    public function produk_kategori($param){
        $kategori = array();
        $produk = $param->param->produk;
        if($produk == ""){
            return $this->response_gagal("02", "Produk tidak ditemukan");die();
        }
        
        // produk kategori
        $query_kategori = "SELECT p.id_produk, k.nama, k.slug, k.id_kategori
                FROM produk_kategori p 
                left join produk pp on p.id_produk = pp.id_produk 
                left join kategori k on p.id_kategori = k.id_kategori 
                where pp.slug = ?
                order by sort_order asc";
        $result_kategori = $this->db_prod->query($query_kategori, $produk);
        if($result_kategori->num_rows() > 0){
            foreach($result_kategori->result() as $row){
                $kategori[] = array(
                    "kategori_id" => $row->id_kategori,
                    "kategori_nama" => $row->nama,
                    "kategori_slug" => $row->slug
                );
            }
            return $this->response_sukses($kategori);die();
        }else{
            return $this->response_gagal("02", "Produk kategori tidak tersedia");die();
        }
    }
    
    public function produk_attribute($param){
        $kategori = array();
        $warna = array();
        $size = array();
        
        $query_kategori = "SELECT * FROM kategori where is_active = '1' and parent_id = 0 order by sort_order asc";
        $result = $this->db_prod->query($query_kategori);
        if($result->num_rows() > 0){
            foreach($result->result() as $row){
                $kategori[] = array(
                    "id" => $row->id_kategori,
                    "nama" => $row->nama,
                    "slug" => $row->slug,
                );
            }
        }
        
        $query_varian = "SELECT v.id_varian, v.nama, v.keterangan, v.is_warna, v.is_active
            FROM produk_varian pv
            INNER JOIN varian v on pv.id_varian = v.id_varian
            where v.is_active = '1' 
            group by v.id_varian
            order by v.id_varian asc";
        $resultvar = $this->db_prod->query($query_varian);
        if($resultvar->num_rows() > 0){
            foreach($resultvar->result() as $row){
                $is_warna = $row->is_warna;
                if($is_warna == "1"){
                    $warna[] = array(
                        "id" => $row->id_varian,
                        "nama" => $row->nama,
                        "keterangan" => $row->keterangan,
                    );
                }else{
                    $is_baju = "0";
                    if(intval($row->nama) > 0){
                        $is_baju = "1";
                    }
                    $size[] = array(
                        "id" => $row->id_varian,
                        "nama" => $row->nama,
                        "keterangan" => $row->keterangan,
                        "tipe" => $is_baju,
                    );
                }
            }
        }
        $resp = array(
            "data_kategori" => $kategori,
            "data_warna" => $warna,
            "data_ukuran" => $size,
        );
        return $this->response_sukses($resp);
    }
    
    
    public function data_produk_detail($param)
    {
        $data = array();
        $result_diskon = "";
        $produk = $param->param->produk;
        $email = $param->param->email;
        
        if($produk == ""){
            return $this->response_gagal("02", "Produk tidak ditemukan");die();
        }
        
        $query = "SELECT p.id_produk, p.sku, p.nama, p.deskripsi, p.harga_coret, p.harga, p.info1, p.info2, p.date_added, p.berat, 
            pso.meta_title, pso.meta_description, pso.meta_keyword,
            sum(pst.stok)as stok
            from produk p
            left join produk_seo pso ON p.id_produk = pso.id_produk
            left join produk_stok pst ON p.id_produk = pst.id_produk
            where p.is_active = '1'
            and p.slug = ?
            limit 1";
        
        $querydiskon = "SELECT p.id_produk, p.sku, p.nama, p.deskripsi, p.harga_coret, p.harga as harga_asli,  p.berat, 
            p.info1, p.info2, p.date_added, 
            pd.id_diskon, pd.harga,
            pso.meta_title, pso.meta_description, pso.meta_keyword
            from produk p
            left join produk_seo pso ON p.id_produk = pso.id_produk
            left join produk_diskon pd ON p.id_produk = pd.id_produk 
            left join customer c ON pd.customer_group_id = c.customer_group_id
            where p.is_active = '1'
            and p.slug = ?
            and pd.date_start <= ?
            and pd.date_end >= ?
            and c.email = ? limit 1";
        
        $result = $this->db_prod->query($query, $produk);
        if($result->num_rows() > 0){
            $row = $result->row();
            $produk_status = 'reguler';
            $produk_diskon = 'n';
            $produk_flash_sale_start = '';
            $produk_flash_sale_end = '';
            $stok_produk = $row->stok;
            $harga_coret = $row->harga_coret;
            $harga = $row->harga;
                
            // cek produk flash sale
            $flash = $this->cek_produk_flash_sale($row->id_produk);
            if($flash->num_rows() > 0){
                $row_flash = $flash->row();
                $stok_produk = intval($row_flash->stok) - intval($row_flash->terjual);
                $produk_status = 'flashsale';
                $produk_diskon = 'y';
                $produk_flash_sale_start = $row_flash->date_start;
                $produk_flash_sale_end = $row_flash->date_end;
                $harga_coret = $row->harga;
                $harga = $row_flash->harga;
            }
            
            $data["produk_id_produk"] = $row->id_produk;
            $data["produk_sku"] = $row->sku;
            $data["produk_nama"] = $row->nama;
            $data["produk_deskripsi"] = $row->deskripsi;
            $data["produk_meta_title"] = $row->meta_title;
            $data["produk_meta_description"] = $row->meta_description;
            $data["produk_meta_keyword"] = $row->meta_keyword;
            $data["produk_diskon"] = $produk_diskon;
            $data["produk_status"] = $produk_status;
            $data["produk_flash_sale_start"] = $produk_flash_sale_start;
            $data["produk_flash_sale_end"] = $produk_flash_sale_end;
            $data["produk_harga"] = $harga;
            $data["produk_harga_coret"] = $harga_coret;
            $data["produk_info1"] = $row->info1;
            $data["produk_info2"] = $row->info2;
            $data["produk_date_added"] = $row->date_added;
            $data["produk_image"] = array();
            $data["produk_image_thumbnail"] = array();
            $data["produk_kategori"] = array();
            $data["produk_varian"] = array();
            $data["produk_shipping"] = array();
            $data["produk_stok"] = $stok_produk;
            $data["produk_stok_detail"] = array();
            $data["produk_weight"] = $row->berat;
            $data["produk_view"] = 0;
            
            if($email != ""){
                $result_diskon = $this->db_prod->query($querydiskon, array($produk, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $email));
                if($result_diskon->num_rows() > 0){
                    $resultDiskon = $result_diskon->row();
                    // ganti harga
                    if($resultDiskon->id_produk != ""){
                        $resultDiskon = $result_diskon->row();
                        $data["produk_diskon"] = "y";
                        $data["produk_harga"] = $resultDiskon->harga;
                        $data["produk_harga_coret"] = $resultDiskon->harga_asli;
                    }
                }
            }
           
            // produk image
            $result_img = $this->db_prod->query("SELECT * FROM produk_image where id_produk = ? order by sort_order asc", array($data["produk_id_produk"]));
            if($result_img->num_rows() > 0){
                foreach($result_img->result() as $row){
                    $data["produk_image"][] = array(
                        "url" => $row->path_image
                    );
                    
                    $data["produk_image_thumbnail"][] = array(
                        "url" => $row->path_image
                    );
                    
                }
            }else{
                $data["produk_image"][] = array(
                    "url" => "http://api.embajeans.store/assets/images/img-coming-soon.png"
                );

                $data["produk_image_thumbnail"][] = array(
                    "url" => "http://api.embajeans.store/assets/images/img-coming-soon.png"
                );
            }
            
            // produk kategori
            $query_kategori = "SELECT p.id_produk, k.nama, k.slug, k.id_kategori
                    FROM produk_kategori p 
                    inner join kategori k on p.id_kategori = k.id_kategori 
                    where p.id_produk = ?
                    order by sort_order asc";
            $result_kategori = $this->db_prod->query($query_kategori, array($data["produk_id_produk"]));
            if($result_kategori->num_rows() > 0){
                foreach($result_kategori->result() as $row){
                    $data["produk_kategori"][] = array(
                        "kategori_id" => $row->id_kategori,
                        "kategori_nama" => $row->nama,
                        "kategori_slug" => $row->slug
                    );
                }
            }
            
            // produk varian
            $query_varian = "SELECT pv.id_produk, pv.sort_order,
                v.id_varian, v.nama, v.keterangan, v.is_warna
                FROM produk_varian pv
                LEFT JOIN varian v ON pv.id_varian = v.id_varian
                where pv.id_produk = ?
                and v.is_active = 1
                order by pv.sort_order asc";
            $result_varian = $this->db_prod->query($query_varian, array($data["produk_id_produk"]));
            if($result_varian->num_rows() > 0){
                foreach($result_varian->result() as $row){
                    $group = "size";
                    $warna = $row->is_warna;
                    if($warna){
                        $group = "color";
                    }
                    
                    $data["produk_varian"][] = array(
                        "id_varian" => $row->id_varian,
                        "nama" => $row->nama,
                        "keterangan" => $row->keterangan,
                        "group" => $group,
                    );
                }
            }
            
            // produk stok detail
            $query_stok = "select p.id_produk, p.id_varian, p.stok, v.is_warna, v.nama, v.keterangan
                from produk_stok p
                left join varian v on p.id_varian = v.id_varian
                where p.id_produk = ?";
            $result_stok = $this->db_prod->query($query_stok, array($data["produk_id_produk"]));
            if($result_stok->num_rows() > 0){
                foreach($result_stok->result() as $row){
                    $data["produk_stok_detail"][] = array(
                        "id_produk" => $row->id_produk,
                        "id_varian" => $row->id_varian,
                        "is_warna" => $row->is_warna,
                        "nama" => $row->nama,
                        "keterangan" => $row->keterangan,
                        "stok" => $row->stok,
                    );
                }
            }
            // cek stok produk yang sudah masuk di keranjang belanja
            if($email != ""){
                $query_cst = "SELECT customer_id FROM customer where email = ? and is_active = 1 limit 1";
                $resultcst = $this->db_prod->query($query_cst, $email);
                if($resultcst->num_rows() > 0){
                    $result = $resultcst->row();
                    $customer_id = $result->customer_id;
                    
                    $produk_stok_cart = array();
                    $querystok = "SELECT c.id_produk, c.qty as total_qty , c.size as id_varian
                            from order_cart c
                            left join produk p on c.id_produk = p.id_produk
                            where p.is_active = '1'
                            and p.slug = ?
                            and c.customer_id = ?";
                    $result_stok = $this->db_prod->query($querystok, array($produk, $customer_id));
                    if($result_stok->num_rows() > 0){
                        foreach($result_stok->result() as $row){
                            $produk_stok_cart[] = array(
                                "id_produk" => $row->id_produk,
                                "id_varian" => $row->id_varian,
                                "belanja" => $row->total_qty,
                            );
                        }
                    }
                    
                    // comparet prosuk stok detail vs produk stok detail cart
                    foreach ($data["produk_stok_detail"] as $i => $defArr) {
                        foreach ($produk_stok_cart as $j => $dayArr) {
                            if ($dayArr['id_produk'] == $defArr['id_produk']) {
                                $stok_update = intval($data["produk_stok_detail"][$i]['stok']);
                                if($data["produk_stok_detail"][$i]['id_varian'] == $produk_stok_cart[$j]['id_varian']){
                                    $stok_update = intval($data["produk_stok_detail"][$i]['stok']) - intval($produk_stok_cart[$j]['belanja']);
                                }
                                $data["produk_stok_detail"][$i]['stok'] = $stok_update;
                            }
                        }
                    }
                    // update produk stok
                    $total_stok = 0;
                    foreach ($data["produk_stok_detail"] as $row_stok) {
                        $total_stok += $row_stok['stok'];
                    }
                    $sisa_stok_data = $total_stok;
                    $data["produk_stok"] = $sisa_stok_data;
                    // cek produk flash sale
                    $flash = $this->cek_produk_flash_sale($data["produk_id_produk"]);
                    if($flash->num_rows() > 0){
                        $row_flash = $flash->row();
                        $stok_flash_sale = intval($row_flash->stok) - intval($row_flash->terjual);
                        if(intval($stok_flash_sale) > 0){
//                            $sisa_stok_data = intval($stok_flash_sale) - $data["produk_stok"];
                            $sisa_stok_data = $stok_flash_sale;
                        }
                    }
                    $data["produk_stok"] = $sisa_stok_data;
                }
            }
            
            $query_shipping = "SELECT * from shipping where is_active = '1' order by sort_order asc";
            $result_shipping = $this->db_prod->query($query_shipping);
            if($result_shipping->num_rows() > 0){
                foreach($result_shipping->result() as $row){
                    $data["produk_shipping"][] = array(
                        "shipping_id" => $row->shipping_id,
                        "shipping_name" => $row->shipping_name
                    );
                }
            }
            // produk view
            $query_view = "SELECT pa.total_view as total FROM produk_attribute pa WHERE id_produk = ? limit 1";
            $result_view = $this->db_prod->query($query_view, $data["produk_id_produk"]);
            if($result_view->num_rows() > 0){
                $row_view = $result_view->row();
                $total_view = intval($row_view->total) + 1;
                
                $data_update = array(
                    "total_view" => $total_view,
                );
                $this->db_prod->update("produk_attribute", $data_update);
                $this->db_prod->where("id_produk", $data["produk_id_produk"]);
                $data["produk_view"] = $total_view;
            }else{
                $data_insert = array(
                    "id_produk" => $data["produk_id_produk"],
                    "total_view" => 1,
                );
                $this->db_prod->insert("produk_attribute", $data_insert);
                $data["produk_view"] = 1;
            }
            return $this->response_sukses($data);
        }else{
            return $this->response_gagal("02", "Produk tidak tersedia");die();
        }
    }
    
    public function shop($param)
    {
        $data = array();
        
        $email = $param->param->email;
        $tipe = $param->param->tipe;
        $kategori = $param->param->kategori;
        $min_price = $param->param->min_price;
        $max_price = $param->param->max_price;
        $color = $param->param->color;
        $size = $param->param->size;
        $page = $param->param->page;
        $limit = $param->param->limit;
        
        if($tipe == ""){
            return $this->response_gagal("02", "Tipe tidak ditemukan");die();
        }
        
        if($page == ""){
            return $this->response_gagal("02", "Page tidak ditemukan");die();
        }
        
        if($limit == ""){
            return $this->response_gagal("02", "Limit tidak ditemukan");die();
        }
        
        $query_count = "SELECT (p.id_produk) from produk p LEFT JOIN produk_stok pst ON p.id_produk = pst.id_produk";
        
        $query = "SELECT p.id_produk, p.sku, p.nama, p.deskripsi, p.harga_coret, p.harga, p.info1, p.info2, p.date_added, p.berat, p.slug, 
            pst.stok
            from produk p
            LEFT JOIN produk_stok pst ON p.id_produk = pst.id_produk";
        
        // join condition
        if($kategori != ""){
            $query_count .= " LEFT JOIN produk_kategori pk ON p.id_produk = pk.id_produk
                LEFT JOIN kategori k on pk.id_kategori = k.id_kategori";
            
            $query .= " LEFT JOIN produk_kategori pk ON p.id_produk = pk.id_produk
                LEFT JOIN kategori k on pk.id_kategori = k.id_kategori";
        }
        
        if($color != "" && $size == ""){
            $query_count .= " LEFT JOIN produk_varian pv ON p.id_produk = pv.id_produk";
            $query .= " LEFT JOIN produk_varian pv ON p.id_produk = pv.id_produk";
        }else if($color == "" && $size != ""){
            $query_count .= " LEFT JOIN produk_varian pv ON p.id_produk = pv.id_produk";
            $query .= " LEFT JOIN produk_varian pv ON p.id_produk = pv.id_produk";
        }else{
            $query_count .= " LEFT JOIN produk_varian pv ON p.id_produk = pv.id_produk";
            $query_count .= " LEFT JOIN produk_varian pvv ON p.id_produk = pvv.id_produk";
            $query .= " LEFT JOIN produk_varian pv ON p.id_produk = pv.id_produk";
            $query .= " LEFT JOIN produk_varian pvv ON p.id_produk = pvv.id_produk";
        }
        
        // group by condition
        $group = "";
        
        // where condition
        $where = " WHERE p.is_active = '1'";
        if($kategori != ""){
            $where .= " and k.id_kategori in($kategori)";
        }
        
        // price condition
        $price = "";
        if($min_price != "" && intval($min_price) > 0){
            $price = " and p.harga >= $min_price";
        }
        
        if($max_price != "" && intval($max_price) > 0){
            $price = " and p.harga <= $max_price";
        }
        
        if($min_price != "" && intval($min_price) > 0 && $max_price != "" && intval($max_price) > 0){
            $price = " and p.harga BETWEEN $min_price AND $max_price";
        }
        
        if(trim($price) != ""){
            $where .= " $price";
        }
        
        // color condition
        $having = "";
        $varian_data = "";
        if($color != ""){
            $varian_data = " and pv.id_varian in ($color)";
            $group = " group by p.id_produk";
        }
        if($size != ""){
            $varian_data = " and pv.id_varian in ($size)";
        }
        if($color != "" && $size != ""){
            //$gab = $color.','.$size;
            //$varian_data = " and pv.id_varian in ($gab)";
            $varian_data = " and pv.id_varian in ($color) and pvv.id_varian in($size)";
            
        }
        if(trim($varian_data) != ""){
            $where .= " $varian_data";
        }
        
        $group = " group by p.id_produk";
        if($tipe == "1"){
            $sort_order = " order by p.date_added DESC";
        }else if($tipe == "2"){
            $sort_order = " order by p.harga ASC";
        }else{
            $sort_order = " order by p.harga DESC";
        }
        
        $group_by = trim($group);
        
        $start_from = ($page-1) * $limit;     
        $lmt = " LIMIT $limit OFFSET $start_from";
        
        // execute
        // get total count
        $query_total = $query_count.$where." ".$group_by;
        $resultCount = $this->db_prod->query($query_total);
        
        $jml_record = 0;
        if(!empty($resultCount) && $resultCount->num_rows() > 0){
            $jml_record = $resultCount->num_rows();
        }else{
            return $this->response_gagal("02", "Produk tidak tersedia");die();
        }
        
        $query = $query.$where." ".$group_by." ".$sort_order.$lmt;
        
        $result = $this->db_prod->query($query);
        if(!empty($resultCount) && $resultCount->num_rows() > 0){
            $jml_record = $resultCount->num_rows();
        }else{
            return $this->response_gagal("02", "Produk tidak tersedia");die();
        }
        
        if(!empty($result) && $result->num_rows() > 0){
            $data_produk = array();
            $list_id_produk = "";
            foreach($result->result() as $row){
                $list_id_produk .= $row->id_produk.",";
                $selisih_diskon = ($row->harga_coret) - ($row->harga);
                $diskon_persen = ($selisih_diskon) / ($row->harga_coret)  * 100;
                $data_produk[] = array(
                    "produk_id" => $row->id_produk,
                    "produk_sku" => $row->sku,
                    "produk_slug" => $row->slug,
                    "produk_nama" => $row->nama,
                    "produk_deskripsi" => "",//$row->deskripsi,
                    "produk_harga" => $row->harga,
                    "produk_harga_coret" => $row->harga_coret == "" ? "0":$row->harga_coret,
                    "produk_date_added" => $row->date_added,
                    "produk_image" => "http://api.embajeans.store/assets/images/img-coming-soon.png",
                    "produk_stok" => $row->stok,
                    "produk_weight" => $row->berat,
                    "produk_diskon_persen" => ceil($diskon_persen),
                );
            }
            $list_id_produk = rtrim($list_id_produk, ",");
            // get image produk
            $query_img = "SELECT pi.id_produk, pi.path_image, pi.sort_order from produk_image pi where id_produk in ($list_id_produk) and sort_order = 1";
            $result_img = $this->db_prod->query($query_img);
            if($result_img->num_rows() > 0){
                foreach($result_img->result() as $rowimg){
                    $data_produk_img[] = array(
                        "produk_id" => $rowimg->id_produk,
                        "image" => $rowimg->path_image,
                        "sort_order" => $rowimg->sort_order,
                    );
                }
                foreach ($data_produk as $i => $defArr) {
                    foreach ($data_produk_img as $j => $dayArr) {
                        if ($dayArr['produk_id'] == $defArr['produk_id']) {
                            $data_produk[$i]['produk_image'] = $data_produk_img[$j]['image'];
                        }
                    }
                }
            }
            $pages = ceil($jml_record / $limit);
            $detail = array(
                "jumlah_record" => $jml_record,
                "current_page" => $page,
                "per_page" => $limit,
                "all_page" => $pages
            );
            return $this->response_sukses($data_produk, $detail);
        }else{
            return $this->response_gagal("02", "Produk tidak tersedia");die();
        }
    }
}