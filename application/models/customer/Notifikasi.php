<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Email model
============================================================== */
class Notifikasi extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function email_order($param)
    {
        if(empty($param->param->order_id)){
            return $this->response_gagal("02", "Order Number tidak tersedia");die();
        }
        if(empty($param->param->order_tipe)){
            return $this->response_gagal("02", "Order Number tidak tersedia");die();
        }
        $order_id = $param->param->order_id;
        $order_tipe = $param->param->order_tipe;
        
        $query = "SELECT o.order_id, o.invoice_no, o.order_payment_id, o.email, 
            o.hp, o.shipping_firstname, o.total, o.weight, o.date_expired,
            p.nama, p.rekening,
            pt.nama as tipe_pembayaran
            FROM `order` o
            LEFT JOIN order_payment p ON o.order_payment_id = p.order_payment_id
            LEFT JOIN order_payment_type pt ON p.order_payment_type_id = pt.order_payment_type_id
            where o.order_id = ? and o.order_status_id = ? LIMIT 1";
        $result = $this->db_prod->query($query, array($order_id, $order_tipe));
        if($result->num_rows() > 0){
            $order_data = $result->row();
            $order_invoice = $order_data->invoice_no;
            $order_payment_id = $order_data->order_payment_id;
            $order_email = $order_data->email;
            $order_hp = $order_data->hp;
            $order_total_pembelian = $order_data->total;
            $order_total_berat = $order_data->weight;
            $order_expired = $order_data->date_expired;
            $order_nama = $order_data->nama;
            $order_rekening = $order_data->rekening;
            $order_tipe_pembayaran = $order_data->tipe_pembayaran;
            
            
            // pembelian baru
            $template = "email_order_baru";
            if($order_tipe == "2"){
                // diproses
            }else if($order_tipe == "3"){
                // dikirim
            }
            $param_email = array(
                "email_to" => $order_email,
                "email_cc" => "",
                "email_bcc" => "",
                "email_subjek" => "Segera Bayar dan Miliki Pesanan Anda sekarang juga",
                "template" => $template,
                "info_text1" => $order_invoice,
                "info_text2" => $order_total_pembelian,
                "info_text3" => $order_tipe_pembayaran,
                "info_text4" => $order_nama." - ".$order_rekening,
                "info_text5" => $order_expired,
            );
            $status = $this->create_email(json_encode($param_email));
            if($status){
                $data["email"] = $order_email;
                return $this->response_sukses($data);
            }else{
                return $this->response_gagal("02","Gagal mengirim email");
            }
        }else{
            return $this->response_gagal("02", "Order tidak tersedia atau sedang di proses");die();
        }
    }
}