<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Ongkir model
============================================================== */
class Ongkir extends MY_Model {
    
    protected $default_origin_id;
    public function __construct() {
        parent::__construct();
        $this->default_origin_id = $this->get_origin_default();
    }
    
    public function cek_ongkir($param){
        $data = array();
        
        if(empty($param->param->destination)){
            return $this->response_gagal("02", "destination tidak tersedia");die();
        }
        
        if(empty($param->param->weight)){
            return $this->response_gagal("02", "berat tidak tersedia");die();
        }
        
        $shipping = array();
        $url = "";
        $key = "";
        $destination = $param->param->destination;
        $weight = $param->param->weight;
        
        
        $result_kurir = $this->db_prod->query("SELECT * FROM shipping where is_active = '1' order by sort_order asc");
        if($result_kurir->num_rows() == 0){
            return $this->response_gagal("02", "kurir tidak tersedia");die();
        }else{
            foreach($result_kurir->result() as $row){
                $shipping[] = array(
                    "shipping_id" => $row->shipping_id,
                    "shipping_name" => $row->shipping_name,
                );
            }
        }
        
        $result_dest = $this->db_prod->query("SELECT area_id, id_relasi from area_shipping where id_relasi = ? and tipe = 2", $destination);
        if($result_dest->num_rows() > 0){
            $result_location = $result_dest->row();
            $destination = $result_location->area_id;
        }else{
            return $this->response_gagal("02", "Destination tidak tersedia");die();
        }
        
        $query2 = "SELECT * from biller where id_biller = 1";
        $result2 = $this->db_prod->query($query2);
        if($result2->num_rows() > 0){
            $shipping_cost = array();
            $resp = array();
            $row_data = $result2->row();
            $url = $row_data->uri;
            $key = $row_data->key;
            
            
            $url = $url."cost";
            foreach($shipping as $row){
                $param = array(
                    "origin" => $this->default_origin_id,
                    "destination" => $destination,
                    "weight" => $weight,
                    "courier" => $row['shipping_id'],
                );
                $response = json_decode($this->send_shipping_post($url, $key, $param));
                if($response->rajaongkir->status->code == "200"){
                    $list = $response->rajaongkir->results;
                    foreach($list as $row){
                        $resp_detail = array();
                        foreach($row->costs as $row_detail){
                            $resp_detail[] = array(
                                "service" => $row_detail->service,
                                "description" => $row_detail->description,
                                "price" => $row_detail->cost[0]->value,
                                "etd" => $row_detail->cost[0]->etd,
                                "note" => $row_detail->cost[0]->note,
                            );
                        }
                        $resp = array(
                            "shipping_id" => $row->code,
                            "shipping_name" => $row->name,
                            "shipping_detail" => $resp_detail
                        );
                    }
                    array_push($shipping_cost, $resp);
                }
            }
            if(!empty($shipping_cost)){
                return $this->response_sukses($shipping_cost);
            }else{
                return $this->response_gagal("02", "Maaf, layanan cek ongkos kirim sedang gangguan");die();
            }
        }else{
            return $this->response_gagal("02", "Maaf, layanan cek ongkos kirim sedang gangguan");die();
        }
    }
    
}