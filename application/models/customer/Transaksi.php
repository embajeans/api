<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Transaksi model
============================================================== */
class Transaksi extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function payment($param){
        $data = array();
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama tidak tersedia");die();
        }
        if(empty($param->param->hp)){
            return $this->response_gagal("02", "Hp tidak tersedia");die();
        }
        if(empty($param->param->propinsi_id)){
            return $this->response_gagal("02", "Propinsi tidak tersedia");die();
        }
        if(empty($param->param->kota_id)){
            return $this->response_gagal("02", "Kota tidak tersedia");die();
        }
        if(empty($param->param->kecamatan_id)){
            return $this->response_gagal("02", "Kecamatan tidak tersedia");die();
        }
        if(empty($param->param->kelurahan_id)){
            return $this->response_gagal("02", "Kelurahan tidak tersedia");die();
        }
        if(empty($param->param->kodepos)){
            return $this->response_gagal("02", "Kodepos tidak tersedia");die();
        }
        if(empty($param->param->address)){
            return $this->response_gagal("02", "Alamat tidak tersedia");die();
        }
        if(empty($param->param->shipping_id)){
            return $this->response_gagal("02", "Shipping ID tidak tersedia");die();
        }
        if(empty($param->param->shipping_service)){
            return $this->response_gagal("02", "Shipping service tidak tersedia");die();
        }
        if(empty($param->param->shipping_price)){
            return $this->response_gagal("02", "Shipping price tidak tersedia");die();
        }
        
        // declare
        $email = $param->param->email;
        $nama = $param->param->nama;
        $hp = $param->param->hp;
        
        $propinsi_id = $param->param->propinsi_id;
        $kota_id = $param->param->kota_id;
        $kecamatan_id = $param->param->kecamatan_id;
        $kelurahan_id = $param->param->kelurahan_id;
        $kodepos = $param->param->kodepos;
        $alamat = $param->param->address;
        $shipping_id = $param->param->shipping_id;
        $shipping_service = $param->param->shipping_service;
        $shipping_price = $param->param->shipping_price;
        $ip_address = $param->param->ip_address;
        
        $query = "SELECT * FROM customer where email = ? and is_active = '1'";
        $result = $this->db_prod->query($query, $email);
        if($result->num_rows() > 0){
            $result = $result->row();
            $customer_id = $result->customer_id;
            // check stok produk
            $stok = $this->get_stok_produk($customer_id);
            if(!empty($stok)){
                $sisa_stok = 0;
                $total_berat = 0;
                foreach(json_decode(json_encode($stok)) as $row){
                    $sisa_stok = $row->produk_stok;
                    if($sisa_stok < 0){
                        return $this->response_gagal("99", "Transaksi gagal, terdapat barang yang stoknya sudah habis");die();
                    }
                    $total_berat =+ $total_berat + $row->produk_berat;
                }
                
                // cek ongkir
                $valid_service = false;
                $valid_service_price = false;
                
                // cek area shipping
                $destination = $param->param->kota_id;
                $result_dest = $this->db_prod->query("SELECT area_id, id_relasi from area_shipping where id_relasi = ? and tipe = 2", $destination);
                if($result_dest->num_rows() > 0){
                    $result_location = $result_dest->row();
                    $destination = $result_location->area_id;
                }else{
                    return $this->response_gagal("02", "Destination tidak tersedia");die();
                }
                
                $ongkir = json_decode($this->cek_ongkir($destination, $total_berat, strtolower($param->param->shipping_id)));
                if(!empty($ongkir)){
                    foreach($ongkir->data as $row){
                        if(strtoupper($row->service) == strtoupper($param->param->shipping_service)){
                            $valid_service = true;
                        }
                        
                        if(intval($row->price) == intval($param->param->shipping_price)){
                            $valid_service_price = true;
                        }
                    }
                }else{
                    return $this->response_gagal("02", "Maaf, layanan cek ongkos kirim sedang gangguan");die();
                }
                
                if($valid_service && $valid_service_price){
                    
                    // update customer
                    // checking nomor hp
                    $nomor_hp = $hp;
                    if(!empty($param->param->hp)){
                        $cek_hp = $this->db_prod->query("SELECT hp from customer where hp = ? limit 1", $nomor_hp);
                        if($cek_hp->num_rows() > 1){
                            $nomor_hp = "";
                        }
                    }else{
                        $nomor_hp = "";
                    }
                    
                    $updatecst = array(
                        "firstname" => $nama,
                        "lastname" => "",
                        "hp" => $nomor_hp,
                    );
                    $this->processing_update_data("customer", "customer_id", $customer_id, $updatecst);
                    
                    
                    $result_address = $this->db_prod->query("SELECT customer_address_id from customer_address where customer_id = ?", $customer_id);
                    if($result_address->num_rows() > 0){
                        $result_address = $result_address->row();
                        $customer_address_id = $result_address->customer_address_id;
                        //  update
                        $qry_update = "update customer_address set 
                            id_propinsi = ?, 
                            id_kota = ?,
                            id_kecamatan = ?,
                            id_kelurahan = ?,
                            kodepos = ?,
                            alamat = ?,
                            is_default = ?,
                            last_update = ?
                            where customer_address_id = ?";
                        $this->db_prod->query($qry_update, array($propinsi_id, $kota_id, $kecamatan_id, $kelurahan_id, $kodepos, $alamat, 1, date('Y-m-d H:i:s'), $customer_address_id));
                    }else{
                        // insert
                        $insertcstaddress = array(
                            "customer_id" => $customer_id,
                            "id_propinsi" => $propinsi_id,
                            "id_kota" => $kota_id,
                            "id_kecamatan" => $kecamatan_id,
                            "id_kelurahan" => $kelurahan_id,
                            "kodepos" => $kodepos,
                            "alamat" => $alamat,
                            "is_default" => 1,
                            "last_update" => date('Y-m-d H:i:s'),
                        );
                        $cust_add_id = $this->processing_insert_data("customer_address", $insertcstaddress);
                        if(!$cust_add_id){
                            return $this->response_gagal("02", "Detail alamat gagal disimpan, silahkan ulangi kembali");die();
                        }
                    }
                    
                    // customer detail update
                    $query_cust_detail = "SELECT c.email, c.customer_group_id,
                        cc.customer_address_id, cc.customer_id, cc.id_propinsi, 
                        cc.id_kota, cc.id_kecamatan, cc.id_kecamatan, cc.id_kelurahan, cc.alamat,
                        p.nama_propinsi,
                        k.nama_kota,
                        kc.nama_kecamatan,
                        kl.nama_kelurahan, kl.kodepos
                        FROM customer c 
                        LEFT JOIN customer_address cc ON c.customer_id = cc.customer_id
                        LEFT JOIN area_propinsi p ON cc.id_propinsi = p.id_propinsi
                        LEFT JOIN area_kota k ON cc.id_kota = k.id_kota
                        LEFT JOIN area_kecamatan kc ON cc.id_kecamatan = kc.id_kecamatan
                        LEFT JOIN area_kelurahan kl ON cc.id_kelurahan = kl.id_kelurahan
                        where c.customer_id = ? LIMIT 1";
                    $cust_detail = $this->db_prod->query($query_cust_detail, $customer_id)->row();
                    
                    // get total order
                    $query_cart = "SELECT c.cart_id, c.session_id, c.qty, c.is_expired, c.id_produk, c.color as color_id,
                        (
                            select v.nama from varian v 
                            left join produk_varian pv on v.id_varian = pv.id_varian
                            where v.id_varian = c.color and v.is_warna = '1' and v.is_active = '1'
                            and pv.id_produk = c.id_produk
                        )as color, c.size as size_id,
                        (
                            select v.nama from varian v 
                            left join produk_varian pv on v.id_varian = pv.id_varian
                            where v.id_varian = c.size and v.is_warna = '0' and v.is_active = '1'
                            and pv.id_produk = c.id_produk
                        )as size, 
                        p.sku, p.nama, p.slug, p.harga, p.harga_coret, pd.harga as harga_diskon, p.berat
                        FROM order_cart c
                        left join produk p on c.id_produk = p.id_produk
                        left join produk_diskon pd ON p.id_produk = pd.id_produk 
                        where c.customer_id = ?
                        and c.is_expired = '0'
                        and p.is_active = '1'
                        group by c.cart_id
                        order by c.cart_id asc";
                    
                    $cart_detail = $this->db_prod->query($query_cart, $customer_id);
                    
                    $list_cart = array();
                    foreach($cart_detail->result() as $row){
                        $total_berat = 0;
                        $total_nominal = 0;
                        $harga_asli = $row->harga;
                        $harga_diskon = $row->harga_diskon;
                        $harga_coret = $row->harga_coret;
                        
                        if($harga_diskon != ""){
                            $harga_coret = $harga_asli;
                            $harga_asli = $harga_diskon;
                        }
                        
                        $total_harga = intval($harga_asli) * intval($row->qty);
                        $total_berat = intval($row->berat) * intval($row->qty);
                        
                        $list_cart[] = array(
                            "cart_id" => $row->cart_id,
                            "cart_qty" => $row->qty,
                            "cart_id_produk" => $row->id_produk,
                            "cart_color_id" => $row->color_id,
                            "cart_color" => $row->color,
                            "cart_size_id" => $row->size_id,
                            "cart_size" => $row->size,
                            "cart_sku" => $row->sku,
                            "cart_nama" => $row->nama,
                            "cart_harga" => $harga_asli,
                            "cart_harga_coret" => $harga_coret,
                            "cart_berat" => $row->berat,
                            "cart_total" => $total_harga,
                            "cart_weight" => $total_berat
                        );
                    }
                    
                    // check produk flash sale
                    $qry_flash = "SELECT c.id_produk, sum(c.qty)as total_qty, 
                        f.flash_sale_id, f.keterangan, f.date_start, f.date_end, p.harga_coret,
                        fp.flash_sale_detail_id, fp.harga_sale as harga, fp.stok, fp.terjual
                        from order_cart c
                        left join produk p on c.id_produk = p.id_produk
                        left join flash_sale_produk fp on c.id_produk = fp.id_produk
                        LEFT JOIN flash_sale f on fp.flash_sale_id = f.flash_sale_id
                        where p.is_active = '1'
                        and c.customer_id = ?
                        and f.date_start <= ?
                        and f.date_end >= ?
                        group by c.id_produk";
                    $flash = $this->db_prod->query($qry_flash, array($customer_id, date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
                    if($flash->num_rows() > 0){
                        foreach($flash->result() as $row_flash){
                            $data_produk_flash[] = array(
                                "cart_id_produk" => $row_flash->id_produk,
                                "cart_harga" => $row_flash->harga,
                                "cart_harga_coret" => $row_flash->harga_coret,
                            );
                        }
                        // compare stok produk asli dengan produk flash sale
                        foreach ($list_cart as $i => $defArr) {
                            foreach ($data_produk_flash as $j => $dayArr) {
                                if ($dayArr['cart_id_produk'] == $defArr['cart_id_produk']) {
                                    $list_cart[$i]['cart_harga'] = $data_produk_flash[$j]['cart_harga'];
                                    $list_cart[$i]['cart_harga_coret'] = $data_produk_flash[$j]['cart_harga_coret'];
                                    $list_cart[$i]['cart_total'] = intval($data_produk_flash[$j]['cart_harga']) * $list_cart[$i]['cart_qty'];
                                }
                            }
                        }
                    }
                    
                    $sum_total_harga = 0;
                    $sum_total_berat = 0;
                    foreach(json_decode(json_encode($list_cart)) as $row_total){
                        $sum_total_berat += $row_total->cart_weight;
                        $sum_total_harga += $row_total->cart_total;
                    }
                    $sum_total_harga = intval($sum_total_harga) +  intval($param->param->shipping_price);
                    // insert order
                    $insert_order = array(
                        "invoice_no" => "EMBA".date('YmdHis').$customer_id,
                        "order_status_id" => 1,
                        "order_payment_id" => 1,
                        "customer_id" => $customer_id,
                        "customer_group_id" => $cust_detail->customer_group_id,
                        "firstname" => $nama,
                        "lastname" => "",
                        "email" => $email,
                        "hp" => $hp,
                        "shipping_firstname" => $nama,
                        "shipping_lastname" => "",
                        "shipping_address_1" => $cust_detail->alamat,
                        "shipping_address_2" => "",
                        "shipping_province_id" => $cust_detail->id_propinsi,
                        "shipping_province_name" => $cust_detail->nama_propinsi,
                        "shipping_city_id" => $cust_detail->id_kota,
                        "shipping_city_name" => $cust_detail->nama_kota,
                        "shipping_kecamatan_id" => $cust_detail->id_kecamatan,
                        "shipping_kecamatan_name" => $cust_detail->nama_kecamatan,
                        "shipping_kelurahan_id" => $cust_detail->id_kelurahan,
                        "shipping_kelurahan_name" => $cust_detail->nama_kelurahan,
                        "shipping_postcode" => $cust_detail->kodepos,
                        "catatan" => "",
                        "weight" => $sum_total_berat,
                        "total" => $sum_total_harga,
                        "ip" => $ip_address,
                        "date_added" => date('Y-m-d H:i:s'),
                        "date_expired" => date('Y-m-d H:i:s',strtotime('+ 12 hour')),
                    );
                    $order_id = $this->processing_insert_data("order", $insert_order);
                    if($order_id){
                        // insert order detail
                        foreach(json_decode(json_encode($list_cart)) as $row_detail){
                            $insert_order_detail = array(
                                "order_id" => $order_id,
                                "id_produk" => $row_detail->cart_id_produk,
                                "nama" => $row_detail->cart_nama,
                                "size" => $row_detail->cart_size_id,
                                "size_name" => $row_detail->cart_size,
                                "color" => $row_detail->cart_color_id,
                                "color_name" => $row_detail->cart_color,
                                "qty" => $row_detail->cart_qty,
                                "harga" => $row_detail->cart_harga,
                                "total" => $row_detail->cart_total,
                                "tax" => 0
                            );
                            $this->processing_insert_data("order_detail", $insert_order_detail);
                        }
                        // insert order shipment
                        $insert_order_shipment = array(
                            "order_id" => $order_id,
                            "shipping_id" => $shipping_id,
                            "shipping_service" => strtoupper($shipping_service),
                            "shipping_price" => $shipping_price,
                            "tracking_number" => ""
                        );
                        $this->processing_insert_data("order_shipment", $insert_order_shipment);
                        
                        // potong stok produk
//                        $qry_stok_produk = "SELECT c.id_produk, sum(c.qty)as total_qty, ps.stok as total_stok,sum(c.qty * p.berat)as total_berat
//                                    from order_cart c
//                                    left join produk p on c.id_produk = p.id_produk
//                                    left JOIN produk_stok ps on p.id_produk = ps.id_produk
//                                    where p.is_active = '1'
//                                    and c.customer_id = ?
//                                    group by c.id_produk";
                        
                        $qry_stok_produk = "SELECT c.id_produk, c.qty, c.size, p.berat
                            from order_cart c
                            left join produk p on c.id_produk = p.id_produk
                            where p.is_active = '1'
                            and c.customer_id = ?
                            group by c.cart_id";

                        $data_produk_cart = array();
                        
                        $list_id = "";
                        $resultstokproduk = $this->db_prod->query($qry_stok_produk, array($customer_id));
                        if($resultstokproduk->num_rows() > 0){
                            foreach($resultstokproduk->result() as $row){
                                $list_id .= $row->id_produk.",";
                                $data_produk_cart[] = array(
                                    "produk_id" => $row->id_produk,
                                    "produk_qty" => $row->qty,
                                    "produk_stok" => 0,
                                    "produk_stok_detail" => 0,
                                    "produk_berat" => $row->berat,
                                    "produk_size" => $row->size,
                                );
                            }
                            $list_id = rtrim($list_id, ",");
                        }
                        
                        // produk stok detail
                        $produk_stok_detail = array();
                        $query_stok = "select p.id_produk, p.id_varian, p.stok, v.is_warna, v.nama, v.keterangan
                            from produk_stok p
                            left join varian v on p.id_varian = v.id_varian
                            where p.id_produk in ($list_id)";

                        $result_stok = $this->db_prod->query($query_stok, array($list_id));
                        if($result_stok->num_rows() > 0){
                            foreach($result_stok->result() as $row){
                                if($row->is_warna == "0"){
                                    $produk_stok_detail[] = array(
                                        "id_produk" => $row->id_produk,
                                        "id_varian" => $row->id_varian,
                                        "is_warna" => $row->is_warna,
                                        "nama" => $row->nama,
                                        "keterangan" => $row->keterangan,
                                        "stok" => $row->stok,
                                    );
                                }
                            }
                        }

                        foreach ($data_produk_cart as $i => $defArr) {
                            foreach ($produk_stok_detail as $j => $dayArr) {
                                if ($dayArr['id_produk'] == $defArr['produk_id']) {
                                    $stok_update = $produk_stok_detail[$j]['stok'];
                                    if($produk_stok_detail[$j]['id_varian'] == $data_produk_cart[$i]['produk_size']){
                                        $stok_update = intval($produk_stok_detail[$j]['stok']) - intval($data_produk_cart[$i]['produk_qty']);
                                        $data_produk_cart[$i]['produk_stok_detail'] = $stok_update;
                                    }

                                }
                            }
                        }
                        
                        // potong stok by ukuran/varian size
                        $sisa_stok = 0;
                        foreach($data_produk_cart as $rowstok){
                            $sisa_stok = $rowstok["produk_stok_detail"];
                            $this->db_prod->query("UPDATE produk_stok set stok = ? where id_produk = ? and id_varian = ?", array($sisa_stok, $rowstok["produk_id"], $rowstok["produk_size"]));
                        }
                        
                        // potong stok produk flash sale
                        $qry_flash = "SELECT c.id_produk, sum(c.qty)as total_qty, 
                            f.flash_sale_id, f.keterangan, f.date_start, f.date_end,
                            fp.flash_sale_detail_id, fp.harga_sale as harga, fp.stok, fp.terjual
                            from order_cart c
                            left join produk p on c.id_produk = p.id_produk
                            left join flash_sale_produk fp on c.id_produk = fp.id_produk
                            LEFT JOIN flash_sale f on fp.flash_sale_id = f.flash_sale_id
                            where p.is_active = '1'
                            and c.customer_id = ?
                            and f.date_start <= ?
                            and f.date_end >= ?
                            group by c.id_produk";
                        $flash = $this->db_prod->query($qry_flash, array($customer_id, date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
                        if($flash->num_rows() > 0){
                            foreach($flash->result() as $row_flash){
                                $stok_terjual = $row_flash->terjual;
                                $qty_pembelian = $row_flash->total_qty;
                                $total_terjual = intval($stok_terjual) + intval($qty_pembelian);
                                $this->db_prod->query("UPDATE flash_sale_produk set terjual = ? where flash_sale_detail_id = ?", array($total_terjual, $row_flash->flash_sale_detail_id));
                            }
                        }
                        // delete cart
                        $this->processing_delete_data("order_cart", "customer_id", $customer_id);
                        
                        $param_detail = array(
                            "order_id" => $order_id
                        );
                        return $this->response_sukses($list_cart, $param_detail);die();
                    }else{
                        return $this->response_gagal("02", "Proses order gagal, silahkan ulangi kembali");die();
                    }
                    
                }else{
                    return $this->response_gagal("02", "Terdapat perbedaan harga ekspedisi, silahkan ulangi kembali.");die();
                }
            }else{
                return $this->response_gagal("02", "Transaksi gagal, keranjang belanja kosong");die();
            }
            
        }else{
            return $this->response_gagal("02", "Email tidak tersedia atau tidak aktif");die();
        }
    }
    
    protected function get_stok_produk($customer_id){
        $stok = array();
        $data_produk_cart = array();
        $data_produk_flash = array();
        $data_produk_stok_global = array();
        $produk_stok_detail = array();
        
        $qry_stok = "SELECT c.id_produk, c.qty, c.size, p.berat
                    from order_cart c
                    left join produk p on c.id_produk = p.id_produk
                    where p.is_active = '1'
                    and c.customer_id = ?
                    group by c.cart_id";
        
        $result = $this->db_prod->query($qry_stok, array($customer_id));
        if($result->num_rows() > 0){
            $list_id = "";
            foreach($result->result() as $row){
                $list_id .= $row->id_produk.",";
                $total_berat = intval($row->qty) * intval($row->berat);
                $data_produk_cart[] = array(
                    "produk_id" => $row->id_produk,
                    "produk_qty" => $row->qty,
                    "produk_stok" => 0,
                    "produk_stok_detail" => 0,
                    "produk_berat" => $total_berat,
                    "produk_size" => $row->size,
                );
            }
            $list_id = rtrim($list_id, ",");
            
            // get produk stok global
            $qry_stok_global = "select p.id_produk, sum(p.stok)as stok_data
                from produk_stok p
                where p.id_produk in ($list_id)
                group by p.id_produk";
            $result_stok_global = $this->db_prod->query($qry_stok_global);
            if($result_stok_global->num_rows() > 0){
                foreach($result_stok_global->result() as $row){
                    $data_produk_stok_global[] = array(
                        "produk_id" => $row->id_produk,
                        "produk_stok" => $row->stok_data,
                    );
                }
            }
            
            // compare
            foreach ($data_produk_cart as $i => $defArr) {
                foreach ($data_produk_stok_global as $j => $dayArr) {
                    if ($dayArr['produk_id'] == $defArr['produk_id']) {
                        $stok_update = intval($data_produk_stok_global[$j]['produk_stok']);
                        $data_produk_cart[$i]['produk_stok'] = $stok_update;
                    }
                }
            }
            
            // produk stok detail
            $produk_stok_detail = array();
            $query_stok = "select p.id_produk, p.id_varian, p.stok, v.is_warna, v.nama, v.keterangan
                from produk_stok p
                left join varian v on p.id_varian = v.id_varian
                where p.id_produk in ($list_id)";
            
            $result_stok = $this->db_prod->query($query_stok, array($list_id));
            if($result_stok->num_rows() > 0){
                foreach($result_stok->result() as $row){
                    if($row->is_warna == "0"){
                        $produk_stok_detail[] = array(
                            "id_produk" => $row->id_produk,
                            "id_varian" => $row->id_varian,
                            "is_warna" => $row->is_warna,
                            "nama" => $row->nama,
                            "keterangan" => $row->keterangan,
                            "stok" => $row->stok,
                        );
                    }
                }
            }
            
            foreach ($data_produk_cart as $i => $defArr) {
                foreach ($produk_stok_detail as $j => $dayArr) {
                    if ($dayArr['id_produk'] == $defArr['produk_id']) {
                        $stok_update = $produk_stok_detail[$j]['stok'];
                        if($produk_stok_detail[$j]['id_varian'] == $data_produk_cart[$i]['produk_size']){
                            $stok_update = intval($produk_stok_detail[$j]['stok']) - intval($data_produk_cart[$i]['produk_qty']);
                            $data_produk_cart[$i]['produk_stok_detail'] = $stok_update;
                        }

                    }
                }
            }
            
            // cek stok produk flash sale
            $qry_flash = "SELECT c.id_produk, sum(c.qty)as total_qty, 
                f.flash_sale_id, f.keterangan, f.date_start, f.date_end,
                fp.flash_sale_detail_id, fp.harga_sale as harga, fp.stok, fp.terjual
                from order_cart c
                left join produk p on c.id_produk = p.id_produk
                left join flash_sale_produk fp on c.id_produk = fp.id_produk
                LEFT JOIN flash_sale f on fp.flash_sale_id = f.flash_sale_id
                where p.is_active = '1'
                and c.customer_id = ?
                and f.date_start <= ?
                and f.date_end >= ?
                group by c.id_produk";
            $flash = $this->db_prod->query($qry_flash, array($customer_id, date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
            if($flash->num_rows() > 0){
                foreach($flash->result() as $row_flash){
                    $stok_produk = intval($row_flash->stok) - intval($row_flash->terjual);
                    $stok_potong = $stok_produk - intval($row_flash->total_qty);
                    $data_produk_flash[] = array(
                        "produk_id" => $row_flash->id_produk,
                        "produk_stok_asli" => $stok_produk,
                        "produk_stok" => $stok_potong,
                    );
                }
                
                // compare stok produk asli dengan produk diskon
                foreach ($data_produk_cart as $i => $defArr) {
                    foreach ($data_produk_flash as $j => $dayArr) {
                        if ($dayArr['produk_id'] == $defArr['produk_id']) {
                            $data_produk_cart[$i]['produk_stok_asli'] = $data_produk_flash[$j]['produk_stok_asli'];
                            $data_produk_cart[$i]['produk_stok'] = $data_produk_flash[$j]['produk_stok'];
                        }
                    }
                }
            }
            $stok = $data_produk_cart;
        }
        return $stok;
    }
    
    protected function cek_ongkir($destination, $weight, $shipping_id){
        $resp_detail = array();
        $query2 = "SELECT * from biller where id_biller = 1";
        $result2 = $this->db_prod->query($query2);
        if($result2->num_rows() > 0){
            $row_data = $result2->row();
            $url = $row_data->uri;
            $key = $row_data->key;
            
            $url = $url."cost";
            $param = array(
                "origin" => $this->get_origin_default(),
                "destination" => $destination,
                "weight" => $weight,
                "courier" => $shipping_id,
            );
            $response = json_decode($this->send_shipping_post($url, $key, $param));
            if($response->rajaongkir->status->code == "200"){
                $list = $response->rajaongkir->results;
                foreach($list as $row){
                    foreach($row->costs as $row_detail){
                        $resp_detail[] = array(
                            "service" => $row_detail->service,
                            "description" => $row_detail->description,
                            "price" => $row_detail->cost[0]->value,
                            "etd" => $row_detail->cost[0]->etd,
                            "note" => $row_detail->cost[0]->note,
                        );
                    }
                }
            }
            
            if(!empty($resp_detail)){
                return $this->response_sukses($resp_detail);
            }else{
                return $this->response_gagal("02", "Maaf, layanan cek ongkos kirim sedang gangguan");die();
            }
        }else{
            return $this->response_gagal("02", "Maaf, layanan cek ongkos kirim sedang gangguan");die();
        }
    }
    
    public function invoice($param){
        $data = array();
        if(empty($param->param->order_id)){
            return $this->response_gagal("02", "Invoice id tidak tersedia");die();
        }
        
        $order_id = $param->param->order_id;
        $query = "SELECT o.order_id, o.invoice_no, o.order_payment_id, o.email, 
            o.hp, o.shipping_firstname, o.total, o.weight, o.date_expired,
            p.nama, p.rekening,
            pt.nama as tipe_pembayaran
            FROM `order` o
            LEFT JOIN order_payment p ON o.order_payment_id = p.order_payment_id
            LEFT JOIN order_payment_type pt ON p.order_payment_type_id = pt.order_payment_type_id
            where o.order_id = ? and ? < date_expired LIMIT 1";
        $result = $this->db_prod->query($query, array($order_id, date("Y-m-d H:i:s")));
        if($result->num_rows() > 0){
            $result = $result->row();
            $data['order_invoice'] = $result->invoice_no;
            $data['order_expired'] = $result->date_expired;
            $data['order_nama'] = $result->shipping_firstname;
            $data['order_email'] = $result->email;
            $data['order_hp'] = $result->hp;
            $data['order_total'] = $result->total;
            $data['order_weight'] = $result->weight;
            $data['order_tipe_pembayaran'] = $result->tipe_pembayaran;
            $data['order_nama_rekening'] = $result->nama;
            $data['order_nomor_rekening'] = $result->rekening;
            return $this->response_sukses($data);die();
        }else{
            return $this->response_gagal("02", "Invoice tidak ditemukan atau sudah kadaluarsa");die();
        }
    }
}