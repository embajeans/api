<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Flash sale model
============================================================== */
class Flashsale extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function flash_sale($param)
    {
        if(!empty($param->param->tipe) != ""){
            $query = "SELECT f.flash_sale_id, f.keterangan, f.date_start, f.date_end, 'open' as status_data
                FROM flash_sale f
                where f.date_start <= ? 
                and f.date_end >= ?
                ORDER BY f.date_start asc limit 3";
            $result = $this->db_prod->query($query, array(date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
        }else{
            $query = "SELECT f.flash_sale_id, f.keterangan, f.date_start, f.date_end,
                (CASE WHEN f.date_start <= ? and f.date_end >= ? THEN 'open'
                ELSE 'closed'
                END) as status_data FROM flash_sale f
                where f.date_end >= ? ORDER BY f.date_start asc limit 3";
            $result = $this->db_prod->query($query, array(date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
        }
            
        if($result->num_rows() > 0){
            $data_sale = array();
            $list_id = "";
            foreach($result->result() as $row){
                $list_id .= $row->flash_sale_id.",";
                $data_sale[] = array(
                    "flash_sale_id" => $row->flash_sale_id,
                    "flash_sale_keterangan" => $row->keterangan,
                    "flash_sale_date_start" => $row->date_start,
                    "flash_sale_date_end" => $row->date_end,
                    "flash_sale_status" => $row->status_data
                );
            }
            $list_id = rtrim($list_id, ",");
            // get produk
            $queryproduk = "SELECT f.flash_sale_id, f.keterangan, f.date_start, f.date_end,
                fp.flash_sale_detail_id, fp.harga_sale as harga, fp.stok, fp.terjual, sum(fp.stok - fp.terjual)as stok_asli,
                p.id_produk, p.sku, p.nama, p.deskripsi, p.harga as harga_coret, p.info1, p.info2, 
                p.date_added, p.berat, p.slug
                from flash_sale_produk fp
                LEFT JOIN flash_sale f on fp.flash_sale_id = f.flash_sale_id
                LEFT JOIN produk p on fp.id_produk = p.id_produk
                WHERE p.is_active = '1'
                and f.flash_sale_id in ($list_id)
                group by fp.flash_sale_detail_id";
            
            $result_produk = $this->db_prod->query($queryproduk);
            if($result_produk->num_rows() > 0){
                $data_produk = array();
                $list_id_produk = "";
                foreach($result_produk->result() as $row){
                    $list_id_produk .= $row->id_produk.",";
                    $selisih_diskon = ($row->harga_coret) - ($row->harga);
                    $diskon_persen = ($selisih_diskon) / ($row->harga_coret)  * 100;
                    $data_produk[] = array(
                        "flash_sale_id" => $row->flash_sale_id,
                        "flash_sale_detail_id" => $row->flash_sale_detail_id,
                        "produk_id" => $row->id_produk,
                        "produk_sku" => $row->sku,
                        "produk_slug" => $row->slug,
                        "produk_nama" => $row->nama,
                        "produk_harga" => $row->harga,
                        "produk_harga_coret" => $row->harga_coret == "" ? "0":$row->harga_coret,
                        "produk_date_added" => $row->date_added,
                        "produk_image" => "",
                        "produk_stok_default" => $row->stok,
                        "produk_jumlah_terjual" => $row->terjual,
                        "produk_stok" => $row->stok_asli,
                        "produk_weight" => $row->berat,
                        "produk_diskon_persen" => ceil($diskon_persen),
                    );
                }
                $list_id_produk = rtrim($list_id_produk, ",");
                
                // get image produk
                $query_img = "SELECT pi.id_produk, pi.path_image, pi.sort_order from produk_image pi where id_produk in ($list_id_produk) and sort_order = 1";
                $result_img = $this->db_prod->query($query_img);
                if($result_img->num_rows() > 0){
                    foreach($result_img->result() as $rowimg){
                        $data_produk_img[] = array(
                            "produk_id" => $rowimg->id_produk,
                            "image" => $rowimg->path_image,
                            "sort_order" => $rowimg->sort_order,
                        );
                    }
                    foreach ($data_produk as $i => $defArr) {
                        foreach ($data_produk_img as $j => $dayArr) {
                            if ($dayArr['produk_id'] == $defArr['produk_id']) {
                                $data_produk[$i]['produk_image'] = $data_produk_img[$j]['image'];
                            }
                        }
                    }
                }
                
                $list_data = array();         
                foreach(json_decode(json_encode($data_sale)) as $row){
                    $flash_id = $row->flash_sale_id;
                    $list_produk = array();
                    foreach(json_decode(json_encode($data_produk)) as $row_detail){
                        if($flash_id == $row_detail->flash_sale_id){
                            $list_produk[] = array(
                                "flash_sale_id" => $row_detail->flash_sale_id,
                                "flash_sale_detail_id" => $row_detail->flash_sale_detail_id,
                                "produk_id" => $row_detail->produk_id,
                                "produk_sku" => $row_detail->produk_sku,
                                "produk_slug" => $row_detail->produk_slug,
                                "produk_nama" => $row_detail->produk_nama,
                                "produk_harga" => $row_detail->produk_harga,
                                "produk_harga_coret" => $row_detail->produk_harga_coret == "" ? "0":$row_detail->produk_harga_coret,
                                "produk_date_added" => $row_detail->produk_date_added,
                                "produk_image" => $row_detail->produk_image,
                                "produk_stok_default" => $row_detail->produk_stok_default,
                                "produk_jumlah_terjual" => $row_detail->produk_jumlah_terjual,
                                "produk_stok" => $row_detail->produk_stok,
                                "produk_weight" => $row_detail->produk_weight,
                                "produk_diskon_persen" => $row_detail->produk_diskon_persen,
                            );
                        }
                    }
                    $list_data[] = array(
                        "flash_sale_id" => $row->flash_sale_id,
                        "flash_sale_keterangan" => $row->flash_sale_keterangan,
                        "flash_sale_date_start" => $row->flash_sale_date_start,
                        "flash_sale_date_end" => $row->flash_sale_date_end,
                        "flash_sale_status" => $row->flash_sale_status,
                        "flash_sale_produk" => $list_produk,
                    );
                }
                return $this->response_sukses($list_data);die();
            }else{
                return $this->response_gagal("02", "Produk flash sale tidak tersedia");die();
            }
            
        }else{
            return $this->response_gagal("02", "Flash sale belum tersedia");die();
        }
    }
    
}