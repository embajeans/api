<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Akun model
============================================================== */
class Akun extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function setting_akun($param){
        $data = array();
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        $email = $param->param->email;
        $query = "SELECT * FROM customer where email = ? and is_active = 1 limit 1";
        $result = $this->db_prod->query($query, $email);
        if($result->num_rows() > 0){
            $result = $result->row();
            $customer_id = $result->customer_id;
            
            $data['customer_id'] = $result->customer_id;
            $data['customer_group_id'] = $result->customer_group_id;
            $data['customer_email'] = $result->email;
            $data['customer_firstname'] = $result->firstname;
            $data['customer_lastname'] = $result->lastname;
            $data['customer_hp'] = $result->hp;
            
            // get data alamat customer
            $data['address'] = array();
            $query_alamat = "SELECT c.customer_address_id, c.customer_id, c.id_propinsi, 
                c.id_kota, c.id_kecamatan, c.id_kecamatan, c.id_kelurahan, c.alamat,
                p.nama_propinsi,
                k.nama_kota,
                kc.nama_kecamatan,
                kl.nama_kelurahan, kl.kodepos
                FROM customer_address c 
                LEFT JOIN area_propinsi p ON c.id_propinsi = p.id_propinsi
                LEFT JOIN area_kota k ON c.id_kota = k.id_kota
                LEFT JOIN area_kecamatan kc ON c.id_kecamatan = kc.id_kecamatan
                LEFT JOIN area_kelurahan kl ON c.id_kelurahan = kl.id_kelurahan
                where c.customer_id = ?";
            $result_alamat = $this->db_prod->query($query_alamat, $customer_id);
            if($result_alamat->num_rows() > 0){
                foreach($result_alamat->result() as $row){
                    $data['address'][] = array(
                        "customer_address_id" => $row->customer_address_id,
                        "customer_id" => $row->customer_id,
                        "customer_propinsi_id" => $row->id_propinsi,
                        "customer_propinsi_name" => $row->nama_propinsi,
                        "customer_city_id" => $row->id_kota,
                        "customer_city_name" => $row->nama_kota,
                        "customer_kecamatan_id" => $row->id_kecamatan,
                        "customer_kecamatan_name" => $row->nama_kecamatan,
                        "customer_kelurahan_id" => $row->id_kelurahan,
                        "customer_kelurahan_name" => $row->nama_kelurahan,
                        "customer_kodepos" => $row->kodepos,
                        "customer_address" => $row->alamat,
                    );
                }
            }
            return $this->response_sukses($data);die();
        }else{
            return $this->response_gagal("02", "Email Anda tidak terdaftar");die();
        }
    }
    public function edit_akun($param){
       
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama tidak tersedia");die();
        }
        if(empty($param->param->hp)){
            return $this->response_gagal("02", "Hp tidak tersedia");die();
        }
        if(empty($param->param->propinsi_id)){
            return $this->response_gagal("02", "Propinsi tidak tersedia");die();
        }
        if(empty($param->param->kota_id)){
            return $this->response_gagal("02", "Kota tidak tersedia");die();
        }
        if(empty($param->param->kecamatan_id)){
            return $this->response_gagal("02", "Kecamatan tidak tersedia");die();
        }
        if(empty($param->param->kelurahan_id)){
            return $this->response_gagal("02", "Kelurahan tidak tersedia");die();
        }
        if(empty($param->param->kodepos)){
            return $this->response_gagal("02", "Kodepos tidak tersedia");die();
        }
        if(empty($param->param->address)){
            return $this->response_gagal("02", "Alamat tidak tersedia");die();
        }
        
        // declare
        $email = $param->param->email;
        $nama = $param->param->nama;
        $hp = $param->param->hp;
        
        $propinsi_id = $param->param->propinsi_id;
        $kota_id = $param->param->kota_id;
        $kecamatan_id = $param->param->kecamatan_id;
        $kelurahan_id = $param->param->kelurahan_id;
        $kodepos = $param->param->kodepos;
        $alamat = $param->param->address;
        
        $query = "SELECT * FROM customer where email = ? and is_active = '1'";
        $result = $this->db_prod->query($query, $email);
        if($result->num_rows() > 0){
            $result = $result->row();
            $customer_id = $result->customer_id;
            // update customer
            // checking nomor hp
            $nomor_hp = $hp;
            if(!empty($param->param->hp)){
                $cek_hp = $this->db_prod->query("SELECT hp from customer where hp = ? limit 1", $nomor_hp);
                if($cek_hp->num_rows() > 1){
                    $nomor_hp = "";
                }
            }else{
                $nomor_hp = "";
            }

            $updatecst = array(
                "firstname" => $nama,
                "lastname" => "",
                "hp" => $nomor_hp,
            );
            $this->processing_update_data("customer", "email", $customer_id, $updatecst);
            
            $result_address = $this->db_prod->query("SELECT customer_address_id from customer_address where customer_id = ?", $customer_id);
            if($result_address->num_rows() > 0){
                $result_address = $result_address->row();
                $customer_address_id = $result_address->customer_address_id;
                //  update
                $qry_update = "update customer_address set 
                    id_propinsi = ?, 
                    id_kota = ?,
                    id_kecamatan = ?,
                    id_kelurahan = ?,
                    kodepos = ?,
                    alamat = ?,
                    is_default = ?,
                    last_update = ?
                    where customer_address_id = ?";
                $this->db_prod->query($qry_update, array($propinsi_id, $kota_id, $kecamatan_id, $kelurahan_id, $kodepos, $alamat, 1, date('Y-m-d H:i:s'), $customer_address_id));
            }else{
                // insert
                $insertcstaddress = array(
                    "customer_id" => $customer_id,
                    "id_propinsi" => $propinsi_id,
                    "id_kota" => $kota_id,
                    "id_kecamatan" => $kecamatan_id,
                    "id_kelurahan" => $kelurahan_id,
                    "kodepos" => $kodepos,
                    "alamat" => $alamat,
                    "is_default" => 1,
                    "last_update" => date('Y-m-d H:i:s'),
                );
                
                $cust_add_id = $this->processing_insert_data("customer_address", $insertcstaddress);
                if(!$cust_add_id){
                    return $this->response_gagal("02", "Detail alamat gagal disimpan, silahkan ulangi kembali");die();
                }
            }
            return $this->response_sukses("");die();
        }else{
            return $this->response_gagal("02", "Email tidak tersedia atau belum aktif");die();
        }
    }
    
    public function transaksi_akun($param){
        if(empty($param->param->email)){
            return $this->response_gagal("02", "Email tidak tersedia");die();
        }
        if(empty($param->param->invoice)){
            return $this->response_gagal("02", "Nomor invoice tidak tersedia");die();
        }
        
        $email = $param->param->email;
        $inv = $param->param->invoice;
        
        $query = "SELECT * FROM customer where email = ? and is_active = '1'";
        $result = $this->db_prod->query($query, $email);
        if($result->num_rows() > 0){
            $result = $result->row();
            $customer_id = $result->customer_id;
            
            $qry_order = "SELECT o.order_id, o.invoice_no, o.total, o.weight, o.date_added, o.date_expired, o.date_modified,
                o.shipping_firstname, o.shipping_address_1, o.shipping_city_name,
                o.shipping_province_name, o.shipping_kecamatan_name, o.shipping_kelurahan_name,
                o.shipping_postcode,
                op.nama as nama_bank, op.rekening,
                os.order_status_id, os.keterangan,
                osp.shipping_id, osp.shipping_service, osp.shipping_price, osp.tracking_number
                FROM `order` o
                left join order_payment op on o.order_payment_id = op.order_payment_id
                left join order_status os on o.order_status_id = os.order_status_id
                left join order_shipment osp on o.order_id = osp.order_id
                where o.invoice_no = ? and o.customer_id = ? LIMIT 1";
            
            $result_order = $this->db_prod->query($qry_order, array($inv, $customer_id));
            if($result_order->num_rows() > 0){
                $row_detail = $result_order->row();
                $order = array(
                    "order_id" => $row_detail->order_id,
                    "invoice_no" => $row_detail->invoice_no,
                    "total" => $row_detail->total,
                    "weight" => $row_detail->weight,
                    "date_added" => $row_detail->date_added,
                    "date_expired" => $row_detail->date_expired,
                    "date_modified" => $row_detail->date_modified,
                    "shipping_firstname" => $row_detail->shipping_firstname,
                    "shipping_address_1" => $row_detail->shipping_address_1,
                    "shipping_city_name" => $row_detail->shipping_city_name,
                    "shipping_province_name" => $row_detail->shipping_province_name,
                    "shipping_kecamatan_name" => $row_detail->shipping_kecamatan_name,
                    "shipping_kelurahan_name" => $row_detail->shipping_kelurahan_name,
                    "shipping_postcode" => $row_detail->shipping_postcode,
                    "nama_bank" => $row_detail->nama_bank,
                    "rekening_bank" => $row_detail->rekening,
                    "status_id" => $row_detail->order_status_id,
                    "status" => $row_detail->keterangan,
                    "shipping_id" => $row_detail->shipping_id,
                    "shipping_service" => $row_detail->shipping_service,
                    "shipping_price" => $row_detail->shipping_price,
                    "tracking_number" => $row_detail->tracking_number
                );
                return $this->response_sukses($order);die();
            }else{
                return $this->response_gagal("02", "Nomor invoice tidak ditemukan");die();
            }
        }else{
            return $this->response_gagal("02", "Email tidak tersedia atau belum aktif");die();
        }
    }
    
    public function konfirmasi_akun($param){
        if(empty($param->param->order_id)){
            return $this->response_gagal("02", "Order id tidak tersedia");die();
        }
        if(empty($param->param->nomor_rekening_pemilik)){
            return $this->response_gagal("02", "Nomor rekening pemilik tidak tersedia");die();
        }
        if(empty($param->param->nama_rekening_pemilik)){
            return $this->response_gagal("02", "Nama rekening pemilik tidak tersedia");die();
        }
        if(empty($param->param->tanggal_transfer))
        {
            return $this->response_gagal("02", "tanggal transfer tidak tersedia");die();
        }
        if(empty($param->param->nomor_rekening_tujuan)){
            return $this->response_gagal("02", "Nomor rekening tujuan tidak tersedia");die();
        }
        
        $email = $param->param->email;
        $order_id = $param->param->order_id;
        $nomor_rekening_pemilik = $param->param->nomor_rekening_pemilik;
        $nama_rekening_pemilik = $param->param->nama_rekening_pemilik;
        $tanggal_transfer = $param->param->tanggal_transfer;
        $nomor_rekening_tujuan = $param->param->nomor_rekening_tujuan;
        
        $query = "SELECT * FROM order_confirmation where order_id = ?";
        $result = $this->db_prod->query($query, $order_id);
        if($result->num_rows() > 0){
            return $this->response_gagal("02", "Konfirmasi Anda sudah masuk, saat ini kami sedang melakukan proses validasi");die();
        }
        // tidak ada, maka insert ke tabel
        $insertdata = array(
            "order_id" => $order_id,
            "nomor_rekening_pemilik" => $nomor_rekening_pemilik,
            "nama_rekening_pemilik" => $nama_rekening_pemilik,
            "tanggal_transfer" => $tanggal_transfer,
            "nomor_rekening_tujuan" => $nomor_rekening_tujuan,
            "date_added" => date('Y-m-d H:i:s'),
            "is_validated" => "0"
        );
        $add_id = $this->processing_insert_data("order_confirmation", $insertdata);
        if(!$add_id){
            return $this->response_gagal("02", "Konfirmasi pembayaran gagal disimpan, silahkan ulangi kembali");die();
        }else{
            return $this->response_sukses("sukses");die();
        }
    }
    
}